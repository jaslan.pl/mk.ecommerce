﻿using System.Reflection;

[assembly: AssemblyProduct("Mała Księgowość \"Rzeczpospolitej\"")]
[assembly: AssemblyCompany("Usługi Informatyczne Andrzej Ciupiński")]
[assembly: AssemblyCopyright("Copyright © 2013-2020")]
[assembly: AssemblyTrademark("Mała Księgowość \"Rzeczpospolitej\"")]
[assembly: AssemblyCulture("")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif