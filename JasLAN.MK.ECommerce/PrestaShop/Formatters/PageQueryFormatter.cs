﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System.Text;

namespace JasLAN.MK.ECommerce.PrestaShop.Formatters
{
    public class PageQueryFormatter : Formatter<PageQuery>
    {
        public override string ToQuery(PageQuery pageQuery)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"limit={pageQuery.Start},{pageQuery.PageSize}");
            foreach (string order in pageQuery.Order)
            {
                sb.Append($"&sort={order}_ASC");
            }

            return sb.ToString();
        }
    }
}
