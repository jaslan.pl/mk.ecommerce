﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JasLAN.MK.ECommerce.PrestaShop.Formatters
{
    [DebuggerDisplay("{ToQuery()}")]
    public class FilterFormatter : Formatter<Filter>
    {    
        public override string ToQuery(Filter filter)
        {
            string value = Convert.ToString(filter.Value);

            if (filter.Operator == "=")
            {
                return $"filter[{HttpUtility.UrlEncode(filter.Field)}]=[{HttpUtility.UrlEncode(value)}]";
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
