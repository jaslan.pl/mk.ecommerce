﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "customer", Namespace = "")]
    public class Customer
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public int Id { get; set; }

        [XmlElement(ElementName = "lastname", Namespace = "")]
        public string Lastname { get; set; }

        [XmlElement(ElementName = "firstname", Namespace = "")]
        public string Firstname { get; set; }

        [XmlElement(ElementName = "email", Namespace = "")]
        public string Email { get; set; }
    }
}
