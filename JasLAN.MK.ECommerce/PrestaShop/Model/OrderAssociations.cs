﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "associations", Namespace = "")]
    public class OrderAssociations
    {
        [XmlArray(ElementName = "order_rows", Namespace = "")]
        [XmlArrayItem(ElementName = "order_row", Namespace = "")]
        public List<OrderRow> OrderRows { get; set; }
    }
}
