﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "order", Namespace = "")]
    public class Order : IOrder
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public int OrderId { get; set; }

        [XmlElement(ElementName = "id_address_delivery")]
        public XLink<Address> AddressDelivery { get; set; }

        [XmlElement(ElementName = "id_address_invoice")]
        public XLink<Address> AddressInvoice { get; set; }

        [XmlElement(ElementName = "id_cart")]
        public XLink<Cart> Cart { get; set; }

        [XmlElement(ElementName = "id_currency")]
        public XLink<Currency> Currency { get; set; }

        [XmlElement(ElementName = "id_lang")]
        public XLink<Language> Lanugage { get; set; }

        [XmlElement(ElementName = "id_customer")]
        public XLink<Customer> Customer { get; set; }

        [XmlElement(ElementName = "id_carrier")]
        public XLink<Carrier> Carrier { get; set; }

        [XmlElement(ElementName = "current_state")]
        public XLink<OrderState> CurrentState { get; set; }

        [XmlElement(ElementName = "module")]
        public string Module { get; set; }

        [XmlElement(ElementName = "invoice_number")]
        public string InvoiceNumber { get; set; }

        [XmlElement(ElementName = "invoice_date")]
        public string InvoiceDate { get; set; }

        [XmlElement(ElementName = "delivery_number")]
        public string DeliveryNumber { get; set; }

        [XmlElement(ElementName = "delivery_date")]
        public string DeliveryDate { get; set; }

        [XmlElement(ElementName = "valid")]
        public bool Valid { get; set; }

        [XmlElement(ElementName = "date_add")]
        public string Date { get; set; }

        [XmlElement(ElementName = "date_upd")]
        public string DateUpd { get; set; }

        [XmlElement(ElementName = "shipping_number")]
        public string ShippingNumber { get; set; }

        [XmlElement(ElementName = "id_shop_group")]
        public string ShopGroup { get; set; }

        [XmlElement(ElementName = "id_shop")]
        public string Shop { get; set; }

        [XmlElement(ElementName = "secure_key")]
        public string SecureKey { get; set; }

        [XmlElement(ElementName = "payment")]
        public string Payment { get; set; }

        [XmlElement(ElementName = "recyclable")]
        public string Recyclable { get; set; }

        [XmlElement(ElementName = "gift")]
        public bool Gift { get; set; }

        [XmlElement(ElementName = "gift_message")]
        public string GiftMessage { get; set; }

        [XmlElement(ElementName = "mobile_theme")]
        public string MobileTheme { get; set; }

        [XmlElement(ElementName = "total_discounts")]
        public decimal TotalDiscounts { get; set; }

        [XmlElement(ElementName = "total_discounts_tax_incl")]
        public decimal TotalDiscountsTaxIncl { get; set; }

        [XmlElement(ElementName = "total_discounts_tax_excl")]
        public decimal TotalDiscountsTaxExcl { get; set; }

        [XmlElement(ElementName = "total_paid")]
        public decimal TotalPaid { get; set; }

        [XmlElement(ElementName = "total_paid_tax_incl")]
        public decimal TotalPaidTaxIncl { get; set; }

        [XmlElement(ElementName = "total_paid_tax_excl")]
        public decimal TotalPaidTaxExcl { get; set; }

        [XmlElement(ElementName = "total_paid_real")]
        public decimal TotalPaidReal { get; set; }

        [XmlElement(ElementName = "total_products")]
        public decimal Total { get; set; }

        [XmlElement(ElementName = "total_products_wt")]
        public decimal TotalProductsWt { get; set; }

        [XmlElement(ElementName = "total_shipping")]
        public decimal TotalShipping { get; set; }

        [XmlElement(ElementName = "total_shipping_tax_incl")]
        public decimal TotalShippingTaxIncl { get; set; }

        [XmlElement(ElementName = "total_shipping_tax_excl")]
        public decimal TotalShippingTaxExcl { get; set; }

        [XmlElement(ElementName = "carrier_tax_rate")]
        public decimal CarrierTaxRate { get; set; }

        [XmlElement(ElementName = "total_wrapping")]
        public decimal TotalWrapping { get; set; }

        [XmlElement(ElementName = "total_wrapping_tax_incl")]
        public decimal TotalWrappingTaxIncl { get; set; }

        [XmlElement(ElementName = "total_wrapping_tax_excl")]
        public decimal TotalWrappingTaxExcl { get; set; }

        [XmlElement(ElementName = "round_mode")]
        public string RoundMode { get; set; }

        [XmlElement(ElementName = "round_type")]
        public string RoundType { get; set; }

        [XmlElement(ElementName = "conversion_rate")]
        public decimal ConversionRate { get; set; }

        [XmlElement(ElementName = "reference")]
        public string Reference { get; set; }

        [XmlElement(ElementName = "associations")]
        public OrderAssociations Associations { get; set; }

        public IAddress BillingAddress
        {
            get
            {
                return this.AddressInvoice?.Instance;
            }
        }

        public IAddress DeliveryAddress
        {
            get
            {
                return this.AddressDelivery?.Instance;
            }
        }

        public string Email
        {
            get
            {
                return this?.Customer?.Instance?.Email;
            }
        }

        public int OrderStatusId
        {
            get
            {
                return Convert.ToInt32(this.CurrentState.Data);
            }
        }

        public string Notes
        {
            get => "";
        }

        public int ProductsCount
        {
            get => this?.Associations?.OrderRows?.Count ?? 0;
        }
    }
}
