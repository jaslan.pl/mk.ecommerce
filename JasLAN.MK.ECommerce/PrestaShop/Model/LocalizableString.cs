﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    public class LocalizableString
    {
        [XmlElement(ElementName = "language", Namespace = "")]
        public List<XLink<Language>> Items { get; set; }

        [XmlIgnore]
        public string Polish
        {
            get
            {
                return this.Items.Where(o => o.Instance.IsoCode == "pl")
                                 .FirstOrDefault()
                                 ?.Data ??
                       this.Items.FirstOrDefault()
                                 ?.Data ??
                       "Brak tłumaczenia";
            }
        }
    }
}
