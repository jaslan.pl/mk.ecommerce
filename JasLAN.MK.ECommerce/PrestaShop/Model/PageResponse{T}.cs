﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    public class PageResponse<T> : IPageResponse<T>
    {
        public int Count { get; set; }

        public int Pages { get; set; }

        public int Page { get; set; }

        public T[] List { get; set; }
    }
}
