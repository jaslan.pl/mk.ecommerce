﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "zone", Namespace = "")]
    public class Zone
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public long Id { get; set; }

        [XmlElement(ElementName = "name")]
        public bool Name { get; set; }

        [XmlElement(ElementName = "active")]
        public bool Active { get; set; }
    }
}
