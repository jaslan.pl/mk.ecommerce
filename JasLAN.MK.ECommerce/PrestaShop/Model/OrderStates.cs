﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "order_states", Namespace = "")]
    public class OrderStates
    {
        [XmlElement(ElementName = "order_state", Namespace = "")]
        public List<XLink<OrderState>> Items { get; set; }
    }
}
