﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "carrier", Namespace = "")]
    public class Carrier
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public long Id { get; set; }

        [XmlElement(ElementName = "name", Namespace = "")]
        public string Name { get; set; }
    }
}
