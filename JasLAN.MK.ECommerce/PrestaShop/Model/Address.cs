﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "address", Namespace = "")]
    public class Address : IAddress
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public string Id { get; set; }

        [XmlElement(ElementName = "id_country", Namespace = "")]
        public XLink<Country> CountryId { get; set; }

        [XmlElement(ElementName = "id_state", Namespace = "")]
        public XLink<State> State { get; set; }

        [XmlElement(ElementName = "alias", Namespace = "")]
        public string Alias { get; set; }

        [XmlElement(ElementName = "company", Namespace = "")]
        public string Company { get; set; }

        [XmlElement(ElementName = "lastname", Namespace = "")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "firstname", Namespace = "")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "vat_number", Namespace = "")]
        public string TaxIdentificationNumber { get; set; }

        [XmlElement(ElementName = "address1", Namespace = "")]
        public string Street1 { get; set; }

        [XmlElement(ElementName = "address2", Namespace = "")]
        public string Street2 { get; set; }

        public string Street
        {
            get
            {
                return $"{this.Street1} {this.Street2}".Trim();
            }
        }

        [XmlElement(ElementName = "postcode", Namespace = "")]
        public string Postcode { get; set; }

        [XmlElement(ElementName = "city", Namespace = "")]
        public string City { get; set; }

        [XmlElement(ElementName = "other", Namespace = "")]
        public string Other { get; set; }

        [XmlElement(ElementName = "phone", Namespace = "")]
        public string Phone { get; set; }

        [XmlElement(ElementName = "phone_mobile", Namespace = "")]
        public string PhoneMobile { get; set; }

        [XmlElement(ElementName = "dni", Namespace = "")]
        public string Dni { get; set; }

        [XmlElement(ElementName = "deleted", Namespace = "")]
        public string Deleted { get; set; }

        public string Country
        {
            get
            {
                return this?.CountryId?.Instance.Name.Polish;
            }
        }
    }
}