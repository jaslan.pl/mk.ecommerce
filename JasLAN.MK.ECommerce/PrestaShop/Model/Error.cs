﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "error", Namespace = "")]
    public class Error
    {
        [XmlElement(ElementName = "code", Namespace = "")]
        public long Code { get; set; }

        [XmlElement(ElementName = "message", Namespace = "")]
        public string Message { get; set; }
    }
}
