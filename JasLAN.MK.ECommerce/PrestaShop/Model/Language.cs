﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "language", Namespace = "")]
    public class Language
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "iso_code")]
        public string IsoCode { get; set; }

        [XmlElement(ElementName = "language_code")]
        public string LanguageCode { get; set; }

        [XmlElement(ElementName = "active")]
        public string Active { get; set; }

        [XmlElement(ElementName = "is_rtl")]
        public string IsRtl { get; set; }

        [XmlElement(ElementName = "date_format_lite")]
        public string DateFormatLite { get; set; }

        [XmlElement(ElementName = "date_format_full")]
        public string DateFormatFull { get; set; }
    }
}
