﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    public class XLink<T> where T : class
    {
        private T instance;

        [XmlAttribute(AttributeName = "id", Namespace = "")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "href", Namespace = "http://www.w3.org/1999/xlink")]
        public string Href { get; set; }

        [XmlText]
        public string Data { get; set; }

        [SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        public T Instance
        {
            get
            {
                if (instance == null)
                {
                    if (string.IsNullOrEmpty(Href))
                    {
                        throw new Exception($"Link for object {Id} {Data} is null or empty");
                    }

                    instance = PrestaShopApi.GetUrl<T>(Href);
                }

                return instance;
            }
        }
    }
}
