﻿using JasLAN.MK.ECommerce.Generic.Model;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "order_row", Namespace = "")]
    public class OrderRow : IOrderProduct
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public int Id { get; set; }

        [XmlElement(ElementName = "product_id")]
        public string ProductId { get; set; }

        [XmlElement(ElementName = "product_attribute_id")]
        public string ProductAttributeId { get; set; }

        [XmlElement(ElementName = "product_quantity")]
        public decimal Quantity { get; set; }

        [XmlElement(ElementName = "product_name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "product_reference")]
        public string ProductReference { get; set; }

        [XmlElement(ElementName = "product_ean13")]
        public string Code { get; set; }

        [XmlElement(ElementName = "product_upc")]
        public string ProductUpc { get; set; }

        [XmlElement(ElementName = "product_price")]
        public decimal Price { get; set; }

        [XmlElement(ElementName = "unit_price_tax_incl")]
        public decimal UnitPriceTaxIncl { get; set; }

        [XmlElement(ElementName = "unit_price_tax_excl")]
        public decimal UnitPriceTaxExcl { get; set; }

        public decimal Discount { get => 0.00m; }

        public string Tax
        {
            get => "";
        }

        public decimal TaxValue
        {
            get
            {
                return this.UnitPriceTaxIncl - this.UnitPriceTaxIncl;
            }
        }

        public string Unit
        {
            get => "";
        }
    }
}