﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "state", Namespace = "")]
    public class State
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public long Id { get; set; }

        [XmlElement(ElementName = "id_zone")]
        public XLink<Zone> Zone { get; set; }

        [XmlElement(ElementName = "id_country")]
        public XLink<Country> Country { get; set; }

        [XmlElement(ElementName = "iso_code")]
        public string IsoCode { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "active")]
        public bool Active { get; set; }
    }
}
