﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "currency", Namespace = "")]
    public class Currency
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public long Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "iso_code")]
        public string IsoCode { get; set; }

        [XmlElement(ElementName = "iso_code_num")]
        public string IsoCodeNum { get; set; }

        [XmlElement(ElementName = "blank")]
        public bool Blank { get; set; }

        [XmlElement(ElementName = "sign")]
        public string Sign { get; set; }

        [XmlElement(ElementName = "format")]
        public int Format { get; set; }

        [XmlElement(ElementName = "decimals")]
        public int Decimals { get; set; }

        [XmlElement(ElementName = "conversion_rate")]
        public decimal ConversionRate { get; set; }

        [XmlElement(ElementName = "deleted")]
        public bool Deleted { get; set; }

        [XmlElement(ElementName = "active")]
        public bool Active { get; set; }
    }
}
