﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "order_state", Namespace = "")]
    public class OrderState : IOrderStatus
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public int Id { get; set; }

        [XmlElement(ElementName = "unremovable")]
        public string Unremovable { get; set; }

        [XmlElement(ElementName = "delivery")]
        public string Delivery { get; set; }

        [XmlElement(ElementName = "hidden")]
        public bool Hidden { get; set; }

        [XmlElement(ElementName = "send_email")]
        public bool SendEmail { get; set; }

        [XmlElement(ElementName = "module_name")]
        public string ModuleName { get; set; }

        [XmlElement(ElementName = "invoice")]
        public string Invoice { get; set; }

        [XmlElement(ElementName = "color")]
        public string Color { get; set; }

        [XmlElement(ElementName = "logable")]
        public string Logable { get; set; }

        [XmlElement(ElementName = "shipped")]
        public string Shipped { get; set; }

        [XmlElement(ElementName = "paid")]
        public string Paid { get; set; }

        [XmlElement(ElementName = "pdf_delivery")]
        public string PdfDelivery { get; set; }

        [XmlElement(ElementName = "pdf_invoice")]
        public string PdfInvoice { get; set; }

        [XmlElement(ElementName = "deleted")]
        public string Deleted { get; set; }

        [XmlElement(ElementName = "name")]
        public LocalizableString LocalizableName { get; set; }

        [XmlElement(ElementName = "template")]
        public LocalizableString Template { get; set; }

        public string Name
        {
            get
            {
                return this?.LocalizableName?.Polish;
            }
        }
    }
}
