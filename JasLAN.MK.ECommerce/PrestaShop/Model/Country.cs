﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "country", Namespace = "")]
    public class Country
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public long Id { get; set; }

        [XmlElement(ElementName = "id_zone")]
        public XLink<Zone> Zone { get; set; }

        [XmlElement(ElementName = "id_currency")]
        public XLink<Currency> Currency { get; set; }

        [XmlElement(ElementName = "call_prefix")]
        public string CallPrefix { get; set; }

        [XmlElement(ElementName = "iso_code")]
        public string IsoCode { get; set; }

        [XmlElement(ElementName = "active")]
        public bool Active { get; set; }

        [XmlElement(ElementName = "contains_states")]
        public bool ContainsStates { get; set; }

        [XmlElement(ElementName = "need_identification_number")]
        public bool NeedIdentificationNumber { get; set; }

        [XmlElement(ElementName = "need_zip_code")]
        public bool NeedZipCode { get; set; }

        [XmlElement(ElementName = "zip_code_format")]
        public string ZipCodeFormat { get; set; }

        [XmlElement(ElementName = "display_tax_label")]
        public bool DisplayTaxLabel { get; set; }

        [XmlElement(ElementName = "name")]
        public LocalizableString Name { get; set; }
    }
}
