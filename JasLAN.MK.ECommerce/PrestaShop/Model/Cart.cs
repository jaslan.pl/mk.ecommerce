﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "cart", Namespace = "")]
    public class Cart
    {
        [XmlElement(ElementName = "id", Namespace = "")]
        public long Id { get; set; }
    }
}
