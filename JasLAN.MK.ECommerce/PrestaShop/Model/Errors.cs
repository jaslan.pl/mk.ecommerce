﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "errors", Namespace = "")]
    public class Errors
    {
        [XmlElement(ElementName = "error", Namespace = "")]
        public List<Error> Items { get; set; }
    }
}
