﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    [XmlRoot(ElementName = "orders", Namespace = "")]
    public class Orders
    {
        [XmlElement(ElementName = "order", Namespace = "")]
        public List<XLink<Order>> Items { get; set; }
    }
}
