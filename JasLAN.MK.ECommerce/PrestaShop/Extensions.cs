﻿using JasLAN.MK.ECommerce.Generic;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.PrestaShop.Model
{
    public static class Extensions
    {
        public static T ReadPrestaShopObject<T>(this string input)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                return ReadPrestaShopObject<T>(memoryStream);
            }
        }

        public static T ReadPrestaShopObject<T>(this Stream stream) 
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), string.Empty);

            using (XmlReader xmlReader = new XmlTextReader(stream))
            {
                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element)
                    {
                        if (xmlReader.Name == "prestashop")
                        {
                            var innerXmlReader = xmlReader.ReadInnerXml();

                            var stringReader = new StringReader(innerXmlReader);

                            XmlNameTable xnt = xmlReader.NameTable;

                            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings
                            {
                                ConformanceLevel = ConformanceLevel.Fragment,
                                NameTable = xnt
                            };

                            return (T)xmlSerializer.Deserialize(XmlReader.Create(stringReader, xmlReaderSettings));
                        }
                    }
                }

                // Rewind stream
                stream.Seek(0, SeekOrigin.Begin);

                return (T)xmlSerializer.Deserialize(stream);
            }
        }

        public static T ReadPrestaShopObject<T>(this WebResponse response)
        {
            string stringResponse = response.ReadString();

            if (stringResponse == "401 Unauthorized")
            {
                throw new Exception("HTTP 401 Unauthorized - Serwer Prestashop może nie być prawidłowo skonfiguorwany. Proszę sprawdzić czy serwer obsługuje moduł Rewrite.");
            }

            return ReadPrestaShopObject<T>(stringResponse);
        }
    }
}
