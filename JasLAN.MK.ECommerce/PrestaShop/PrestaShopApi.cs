﻿using JasLAN.MK.ECommerce.Generic;
using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using JasLAN.MK.ECommerce.PrestaShop.Formatters;
using JasLAN.MK.ECommerce.PrestaShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace JasLAN.MK.ECommerce.PrestaShop
{
    /// <summary>
    /// http://doc.prestashop.com/display/PS16/Web+service+reference
    /// </summary>
    public class PrestaShopApi : IApi
    {
        public FormattersCollection Formatters {
            get
            {
                return new FormattersCollection(
                    new Dictionary<Type, Func<IFormatter>> {
                        { typeof(Filter), () => new FilterFormatter() },
                        { typeof(FilterQuery), () => new FilterQueryFormatter() },
                        { typeof(PageQuery), () => new PageQueryFormatter() } }
                );
            }
        }

        private string url;

        public PrestaShopApi()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public static string AppKey { get; set; }

        public string Url
        {
            get
            {
                return url;
            }

            set
            {
                url = value;

                if (!string.IsNullOrEmpty(url))
                {
                    if (!url.ToLower().StartsWith("https://") &&
                        !url.ToLower().StartsWith("http://"))
                    {
                        url = $"https://{url}";
                    }
                }
            }
        }

        public bool Login(string login, string password)
        {
            AppKey = login;
            this.GetOrderStatuses(new PageQuery(), new FilterQuery());
            return true;
        }

        public IPageResponse<IOrder> GetOrders(PageQuery pageQuery = null, FilterQuery filterQuery = null, int orderStatusId = 0)
        {
            IPageResponse<IOrder> pageResponse = new PageResponse<IOrder>();

            if (orderStatusId != 0)
            {
                filterQuery.Filters.Add(new Filter("current_state", orderStatusId));
            }

            pageQuery.MapToApiProperties<Order>();

            var resource = $"orders";

            var countOrders = this.Get<Orders>(resource, filterQuery);
            pageResponse.Count = countOrders.Items.Count;

            var orders = this.Get<Orders>(resource, filterQuery, pageQuery);
            pageResponse.Page = pageQuery.PageNumber;
            pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
            pageResponse.List = orders.Items.Select(o => o.Instance)
                                            .ToArray();
            return pageResponse;
        }

        public IPageResponse<IOrderProduct> GetOrderProducts(int orderId, PageQuery pageQuery = null)
        {
            IPageResponse<IOrderProduct> pageResponse = new PageResponse<IOrderProduct>();

            var resource = $"orders/{orderId}";

            var order = this.Get<Order>(resource);

            var orderProducts = order.Associations.OrderRows;

            pageResponse.Count = orderProducts.Count;

            int pageStart = pageQuery.Start;
            pageResponse.Page = pageQuery.PageNumber;
            pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
            pageResponse.List = orderProducts.Skip(pageStart)
                                             .Take(pageQuery.PageSize)
                                             .ToArray();
            return pageResponse;
        }

        public IPageResponse<IOrderStatus> GetOrderStatuses(PageQuery pageQuery, FilterQuery filterQuery)
        {
            IPageResponse<IOrderStatus> pageResponse = new PageResponse<IOrderStatus>();

            var resource = $"order_states";

            var countOrderStates = this.Get<OrderStates>(resource, filterQuery);
            pageResponse.Count = countOrderStates.Items.Count;      

            var orderStates = this.Get<OrderStates>(resource, filterQuery, pageQuery);
            pageResponse.Page = pageQuery.PageNumber;
            pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
            pageResponse.List = orderStates.Items.Select(o => o.Instance)
                                                 .ToArray();
            return pageResponse;
        }

        public static T GetUrl<T>(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(PrestaShopApi.AppKey + ":")));

            try
            {
                WebResponse response = request.GetResponse();

                return response.ReadPrestaShopObject<T>();
            }
            catch (WebException ex)
            {
                try
                {
                    var errors = ex.Response.ReadPrestaShopObject<Errors>();

                    if (errors.Items.Any())
                    {
                        throw new WebException(errors.Items.FirstOrDefault().Message, ex, ex.Status, ex.Response);
                    }
                }
                catch (WebException)
                {
                    throw;
                }

                throw;
            }
        }

        private T Get<T>(string resource, params IUrlFormattable[] formattables)
        {
            if (Url == null)
            {
                throw new InvalidOperationException("Url not set");
            }

            return GetUrl<T>(this.MakeQuery($"{Url}/api/{resource}", formattables));
        }

        public IPageResponse<IInvoice> GetInvoices(PageQuery pageQuery, FilterQuery filterQuery)
        {
            throw new NotImplementedException();
        }

        public IPageResponse<IProduct> GetProducts(bool onlyActive, PageQuery pageQuery, FilterQuery filterQuery)
        {
            throw new NotImplementedException();
        }

        public IProduct GetProductByCode(string code)
        {
            throw new NotImplementedException();
        }

        public IProduct GetProductByEan(string ean)
        {
            throw new NotImplementedException();
        }

        public bool UpdateProductStock(int productId, int stockId, decimal stock)
        {
            throw new NotImplementedException();
        }
    }
}
