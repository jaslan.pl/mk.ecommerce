﻿using JasLAN.MK.ECommerce.Generic;
using JasLAN.MK.ECommerce.SkyShop.Model;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace JasLAN.MK.ECommerce.Skyshop
{   
    /// <summary>
    /// Extension class
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Read object from string implementation
        /// </summary>
        /// <typeparam name="T">Type of object to read</typeparam>
        /// <param name="input">Input string</param>
        /// <returns>Object read</returns>
        public static T ReadSkyshopObject<T>(this string input)
        {
            int responseCode = 0;
            bool hasResponseCode = typeof(IHasResponseCode).IsAssignableFrom(typeof(T));

            if (hasResponseCode)
            {
                if (input.Contains("\"response_code\": 200,"))
                {
                    input = input.Replace("\"response_code\": 200,", "");
                    responseCode = 200;
                }

                if (input.Contains("\"response_code\": 200"))
                {
                    input = input.Replace("\"response_code\": 200", "");
                    responseCode = 200;
                }
            }

            using (MemoryStream memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(input)))
            {
                var @object = ReadSkyshopObject<T>(memoryStream);

                if (hasResponseCode)
                {
                    ((IHasResponseCode)@object).ResponseCode = responseCode;
                }

                return @object;
            }
        }

        /// <summary>
        /// Read object from stream
        /// </summary>
        /// <typeparam name="T">Type of object to read</typeparam>
        /// <param name="stream">Stream to read from</param>
        /// <returns>Object read</returns>
        private static T ReadSkyshopObject<T>(this Stream stream)
        {
            var settings = new DataContractJsonSerializerSettings()
            {
                UseSimpleDictionaryFormat = true
            };

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), settings);
            return (T)serializer.ReadObject(stream);
        }

        /// <summary>
        /// Read object from <paramref name="response"/>
        /// </summary>
        /// <typeparam name="T">Type of object to read</typeparam>
        /// <param name="response">Response to read from</param>
        /// <returns>Object read</returns>
        public static T ReadSkyShopObject<T>(this WebResponse response)
        {
            string stringResponse = response.ReadString();

            return ReadSkyshopObject<T>(stringResponse);
        }
    }
}
