﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Diagnostics;
using System.Web;
using JasLAN.MK.ECommerce.Generic;
using JasLAN.MK.ECommerce.Generic.Model;
using JasLAN.MK.ECommerce.Generic.Formatters;
using System.Reflection;
using System.Xml.Serialization;
using System.Collections.Specialized;
using JasLAN.MK.ECommerce.SkyShop.Model;
using JasLAN.MK.ECommerce.Skyshop;

namespace JasLAN.MK.ECommerce.SkyShop
{
    /// <summary>
    /// http://mk.mysky-shop.pl/api
    /// </summary>
    public class SkyShopApi : IApi
    {       
        private string url;

        public SkyShopApi()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;            
        }

        public static string AppKey { get; set; }

        public string Url
        {
            get
            {
                return url;
            }

            set
            {
                url = value;

                url = url.TrimEnd('/');

                if (!string.IsNullOrEmpty(url))
                {
                    if (!url.ToLower().StartsWith("https://") &&
                        !url.ToLower().StartsWith("http://"))
                    {
                        url = $"https://{url}";
                    }
                }
            }
        }

        public FormattersCollection Formatters
        {
            get
            {
                return new FormattersCollection();
            }
        }

        public bool Login(string login, string password)
        {
            AppKey = login;
            this.GetOrderStatuses(new PageQuery(), new FilterQuery());
            return true;
        }

        public IPageResponse<IOrder> GetOrders(PageQuery pageQuery = null, FilterQuery filterQuery = null, int orderStatusId = 0)
        {
            IPageResponse<IOrder> pageResponse = new PageResponse<IOrder>();

            if (orderStatusId != 0)
            {
                filterQuery.Filters.Add(new Filter("ord_status", orderStatusId));
            }

            var resource = $"?function=getOrders";

            var countOrders = this.Post<Orders>(resource, filterQuery);
            pageResponse.Count = countOrders.Count;

            var orders = this.Post<Orders>(resource, filterQuery, pageQuery);
            pageResponse.Page = pageQuery.PageNumber;
            pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
            pageResponse.List = orders.Values.ToArray();
            return pageResponse;
        }

        public IPageResponse<IOrderProduct> GetOrderProducts(int orderId, PageQuery pageQuery = null)
        {
            IPageResponse<IOrderProduct> pageResponse = new PageResponse<IOrderProduct>();

            var filterQuery = new FilterQuery();
            filterQuery.Filters.Add(new Filter("ord_id", orderId));

            var resource = $"?function=getOrders";

            var countOrders = this.Post<Orders>(resource, filterQuery);
            pageResponse.Count = countOrders.Count;

            var orders = this.Post<Orders>(resource, filterQuery, pageQuery);

            var order = orders.Values.First();

            if (order == null)
            {
                pageResponse.Page = 1;
                pageResponse.Pages = 1;
                pageResponse.List = new OrderProduct[0];
            }
            else
            {
                pageResponse.Page = pageQuery.PageNumber;
                pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
                pageResponse.List = order.Products.Skip(pageQuery.Start)
                                                  .Take(pageQuery.PageSize)
                                                  .ToArray();
            }

            return pageResponse;
        }

        public IPageResponse<IOrderStatus> GetOrderStatuses(PageQuery pageQuery, FilterQuery filterQuery)
        {
            IPageResponse<IOrderStatus> pageResponse = new PageResponse<IOrderStatus>
            {
                Count = OrderStatus.Repository.Count
            };

            var orderStates = OrderStatus.Repository.Skip(pageQuery.Start)
                                                    .Take(pageQuery.PageSize);
            pageResponse.Page = pageQuery.PageNumber;
            pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
            pageResponse.List = orderStates.ToArray();
            return pageResponse;
        }

        public IPageResponse<IInvoice> GetInvoices(PageQuery pageQuery, FilterQuery filterQuery)
        {
            IPageResponse<IInvoice> pageResponse = new PageResponse<IInvoice>();

            var resource = $"?function=getInvoices";

            var invoicesCount = this.Post<Invoices>(resource, filterQuery);
            pageResponse.Count = invoicesCount.Count;

            var invoices = invoicesCount.Values.Skip(pageQuery.Start).Take(pageQuery.PageSize).ToList();
            pageResponse.Page = pageQuery.PageNumber;
            pageResponse.Pages = (int)Math.Ceiling((decimal)pageResponse.Count / pageQuery.PageSize);
            pageResponse.List = invoices.ToArray();
            return pageResponse;
        }

        public static T PostUrl<T>(string url, IDictionary<string, object> values)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            NameValueCollection formData = HttpUtility.ParseQueryString(string.Empty);
            formData.Add("APIkey", SkyShopApi.AppKey);
            foreach (var pair in values)
            {
                formData.Add(pair.Key, Convert.ToString(pair.Value));
            }
            string content = formData.ToString();

            // Convert the post string to a byte array
            byte[] bytedata = Encoding.UTF8.GetBytes(content);
            request.ContentLength = bytedata.Length;

            // Create the stream
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();

            try
            {
                WebResponse response = request.GetResponse();

                return response.ReadSkyShopObject<T>();
            }
            catch (WebException ex)
            {
                try
                {
                    var errors = ex.Response.ReadSkyShopObject<Errors>();

                    throw;                  
                }
                catch (WebException)
                {
                    throw;
                }

                throw;
            }
        }

        private T Post<T>(string resource, FilterQuery filterQuery)
        {
            var values = new Dictionary<string, object>();

            ApplyPageQuery(values, new PageQuery(1, 100));
            ApplyFilterQuery(values, filterQuery);

            return Post<T>(resource, values);
        }

        private T Post<T>(string resource, FilterQuery filterQuery, PageQuery pageQuery)
        {
            var values = new Dictionary<string, object>();

            ApplyPageQuery(values, pageQuery);
            ApplyFilterQuery(values, filterQuery);
            
            return Post<T>(resource, values);
        }

        private static void ApplyPageQuery(IDictionary<string, object> values, PageQuery pageQuery)
        {
            values.Add("start", pageQuery.Start);
            values.Add("limit", pageQuery.PageSize);
        }

        private void ApplyFilterQuery(IDictionary<string, object> values, FilterQuery filterQuery)
        {
            foreach (var filter in filterQuery.Filters)
            {
                switch (filter.Field)
                {
                    case "dateStart":
                    case "dateEnd":
                        values.Add(filter.Field, filter.Value);
                        break;
                    case "ord_status":
                        var orderStatus = OrderStatus.Repository.FirstOrDefault(o => o.Id == Convert.ToInt32(filter.Value));

                        if (orderStatus != null)
                        {
                            values.Add("search", $"ord_status={orderStatus.Code}");
                        }
                        break;
                    default:
                        values.Add("search", $"{filter.Field}={filter.Value}");
                        break;
                }
            }
        }

        private T Post<T>(string resource, IDictionary<string, object> values)
        {
            if (Url == null)
            {
                throw new InvalidOperationException("Url not set");
            }

            return PostUrl<T>($"{Url}/api/{resource}", values);
        }

        public IPageResponse<IProduct> GetProducts(bool onlyActive, PageQuery pageQuery, FilterQuery filterQuery)
        {
            throw new NotImplementedException();
        }

        public IProduct GetProductByCode(string code)
        {
            throw new NotImplementedException();
        }

        public IProduct GetProductByEan(string ean)
        {
            throw new NotImplementedException();
        }

        public bool UpdateProductStock(int productId, int stockId, decimal stock)
        {
            throw new NotImplementedException();
        }
    }
}
