﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [Serializable]
    public sealed class Invoices : Dictionary<string, Invoice>, IHasResponseCode
    {
        public Invoices()
        {
        }

        private Invoices(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        [DataMember(Name = "response_code")]
        public int ResponseCode { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
