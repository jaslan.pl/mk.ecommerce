﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [DataContract]
    public class Invoice : IInvoice
    {        
        private string data;
        private ICollection<InvoiceProduct> products;

        [DataMember(Name = "inv_id")]
        public int InvoiceId { get; set; }

        [DataMember(Name = "inv_number_string")]
        public string Number { get; set; }
        
        [DataMember(Name = "inv_date")]
        public string Date { get; set; }

        [DataMember(Name = "inv_data")]
        public string Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
                products = null;
            }
        }

        [DataMember(Name = "inv_sell_date")]
        public string SaleDate { get; set; }

        [DataMember(Name = "inv_bill_company")]
        public string BillingCompany { get; set; }

        [DataMember(Name = "inv_bill_vat")]
        public string BillingVat { get; set; }

        [DataMember(Name = "inv_bill_city")]
        public string BillingCity { get; set; }

        [DataMember(Name = "inv_bill_country")]
        public string BillingCountry { get; set; }

        [DataMember(Name = "inv_price")]
        public decimal Total { get; set; }

        [DataMember(Name = "inv_price_net")]
        public decimal TotalNet { get; set; }

        [DataMember(Name = "inv_tax")]
        public decimal TotalTax { get; set; }

        [DataMember(Name = "inv_bill_street")]
        public string BillingStreet { get; set; }

        [DataMember(Name = "inv_bill_code")]
        public string BillingCode { get; set; }

        [DataMember(Name = "inv_payment_other")]
        public string PaymentForm { get; set; }

        [DataMember(Name = "inv_prices_type")]
        public string PriceType { get; set; }

        public IAddress BillingAddress
        {
            get
            {
                return new Address()
                {
                    Company = BillingCompany,
                    TaxIdentificationNumber = BillingVat,
                    City = BillingCity,
                    Country = BillingCountry,
                    Street = BillingStreet,
                    Postcode = BillingCode,
                };
            }
        }

        [IgnoreDataMember]
        public ICollection<InvoiceProduct> Products
        {
            get
            {
                if (products == null)
                {
                    if (this.data != null)
                    {
                        try
                        {
                            using (MemoryStream memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(this.data)))
                            {
                                var settings = new DataContractJsonSerializerSettings()
                                {
                                    UseSimpleDictionaryFormat = true
                                };

                                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ICollection<InvoiceProduct>), settings);
                                products = (ICollection<InvoiceProduct>)serializer.ReadObject(memoryStream);
                            }
                        }
                        catch
                        {
                            // Ignore errors
                        }
                    }
                }

                return products;
            }
        }

        [IgnoreDataMember]    
        public decimal Net23
        {
            get
            {
                return Net(23);
            }
        }

        [IgnoreDataMember]
        public decimal Net8
        {
            get
            {
                return Net(8);
            }
        }

        [IgnoreDataMember]
        public decimal Vat23
        {
            get
            {
                return Vat(23);
            }
        }

        [IgnoreDataMember]
        public decimal Vat8
        {
            get
            {
                return Vat(8);
            }
        }

        private decimal Net(decimal tax)
        {
            if (this.Products == null)
            {
                return 0m;
            }

            if (PriceType == "gross")
            {
                var net = this.Products.Where(o => Convert.ToInt32(o.Tax) == tax)
                                       .Select(o => Math.Round((o.PriceGross * o.Quantity), 2))
                                       .Sum() / (1 + tax / 100);

                return Math.Round(net, 2);
            }

            return this.Products.Where(o => Convert.ToInt32(o.Tax) == tax)
                                .Select(o => o.Price * o.Quantity)
                                .Sum();
        }

        private decimal Vat(decimal tax)
        {
            if (this.Products == null)
            {
                return 0m;
            }

            if (PriceType == "gross")
            {
                var gross = this.Products.Where(o => Convert.ToInt32(o.Tax) == tax)
                                         .Select(o => (o.PriceGross * o.Quantity))
                                         .Sum();

                return Math.Round(gross, 2) - Net(tax);
            }

            return this.Products.Where(o => Convert.ToInt32(o.Tax) == tax)
                                     .Select(o => o.TaxValue * o.Quantity)
                                     .Sum();
        }
    }
}
