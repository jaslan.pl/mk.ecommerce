﻿using JasLAN.MK.ECommerce.Generic.Model;
using System.Runtime.Serialization;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [DataContract]
    public class OrderProduct : IOrderProduct
    {
        [DataMember(Name = "op_amount")]
        public string Amount { get; set; }

        [DataMember(Name = "op_price_net")]
        public decimal Price { get; set; }

        [DataMember(Name = "op_price")]
        public decimal PriceGross { get; set; }

        public decimal Discount { get; set; }

        public decimal Quantity
        {
            get
            {
                decimal.TryParse(this.Amount, out decimal quantity);
                return quantity;
            }
        }

        [DataMember(Name = "op_prod_name_pl")]
        public string Name { get; set; }

        [DataMember(Name = "op_symbol")]
        public string Code { get; set; }

        [DataMember(Name = "op_prod_tax")]
        public string Tax { get; set; }

        public decimal TaxValue
        {
            get
            {
                return PriceGross - Price;
            }
        }

        [DataMember(Name = "op_prod_unit")]
        public string Unit { get; set; }
    }
}
