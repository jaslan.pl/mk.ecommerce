﻿using JasLAN.MK.ECommerce.Generic.Model;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    public class PageResponse<T> : IPageResponse<T>
    {
        public int Count { get; set; }

        public int Pages { get; set; }

        public int Page { get; set; }

        public T[] List { get; set; }
    }
}
