﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [DataContract]
    public class Order : IOrder
    {
        [DataMember(Name = "ord_paid")]
        public bool IsPaid { get; set; }

        [DataMember(Name = "ord_id")]
        public int OrderId { get; set; }

        [DataMember(Name = "ord_user_id")]
        public int? UserId { get; set; }

        [DataMember(Name = "ord_date")]
        public string Date { get; set; }

        public string DeliveryDate { get; set; }

        public int OrderStatusId
        {
            get
            {
                return (Model.OrderStatus.Repository.FirstOrDefault(o => o.Code == OrderStatus) ??
                        Model.OrderStatus.Repository.FirstOrDefault()).Id;
            }
        }

        [DataMember(Name = "ord_status")]
        public string OrderStatus { get; set; }

        [DataMember(Name = "ord_email")]
        public string Email { get; set; }

        [DataMember(Name = "ord_note")]
        public string Notes { get; set; }

        public decimal Total
        {
            get
            {
                if (Products == null)
                {
                    return this.TotalShipping;
                }

                return Products.Sum(o => o.Quantity * o.PriceGross) + this.TotalShipping;
            }
        }

        public decimal TotalPaid
        {
            get
            {
                if (IsPaid)
                {
                    return Total;
                }

                return 0;
            }
        }

        /// <summary>
        /// Shiping price gross -> net = ord_ship_price_net
        /// </summary>
        [DataMember(Name = "ord_ship_price")]
        public decimal TotalShipping { get; set; }

        public int ProductsCount
        {
            get
            {
                if (Products == null)
                {
                    return 0;
                }

                return Products.Count;
            }
        }

        [DataMember(Name = "ord_bill_company")]
        public string BillingCompany { get; set; }

        [DataMember(Name = "ord_bill_vat")]
        public string BillingVat { get; set; }

        [DataMember(Name = "ord_bill_city")]
        public string BillingCity { get; set; }

        [DataMember(Name = "ord_bill_country")]
        public string BillingCountry { get; set; }

        [DataMember(Name = "ord_bill_street")]
        public string BillingStreet { get; set; }

        [DataMember(Name = "ord_bill_code")]
        public string BillingCode { get; set; }

        public IAddress BillingAddress
        {
            get
            {
                return new Address()
                {
                    Company = BillingCompany,
                    TaxIdentificationNumber = BillingVat,
                    City = BillingCity,
                    Country = BillingCountry,
                    Street = BillingStreet,
                    Postcode = BillingCode,
                };
            }
        }

        [DataMember(Name = "ord_company")]
        public string Company { get; set; }

        [DataMember(Name = "ord_firstname")]
        public string Firstname { get; set; }

        [DataMember(Name = "ord_lastname")]
        public string Lastname { get; set; }

        [DataMember(Name = "ord_phone")]
        public string Phone { get; set; }

        [DataMember(Name = "ord_street")]
        public string Street { get; set; }

        [DataMember(Name = "ord_city")]
        public string City { get; set; }

        [DataMember(Name = "ord_country")]
        public string Country { get; set; }

        [DataMember(Name = "ord_code")]
        public string Code { get; set; }

        public IAddress DeliveryAddress
        {
            get
            {
                return new Address()
                {
                    City = City,
                    Company = Company,
                    Country = Country,
                    FirstName = Firstname,
                    LastName = Lastname,
                    Phone = Phone,
                    Postcode = Code,
                    Street = Street,
                };
            }
        }

        [DataMember(Name = "products")]
        public ICollection<OrderProduct> Products { get; set; }
    }
}
