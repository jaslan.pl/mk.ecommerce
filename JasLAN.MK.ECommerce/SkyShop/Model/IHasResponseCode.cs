﻿namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    public interface IHasResponseCode
    {
        int ResponseCode { get; set; }
    }
}
