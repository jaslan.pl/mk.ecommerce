﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    public class OrderStatus : IOrderStatus
    {
        public static readonly ICollection<OrderStatus> Repository;

        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        static OrderStatus()
        {
            Repository = new List<OrderStatus>()
            {
                new OrderStatus()
                {
                    Id = 0,
                    Code = "new",
                    Name = "Nowe"
                },
                new OrderStatus()
                {
                    Id = 1,
                    Code = "canceled",
                    Name = "Anulowane"
                },
                new OrderStatus()
                {
                    Id = 2,
                    Code = "waiting_for_send",
                    Name = "Spakowane"
                },
                new OrderStatus()
                {
                    Id = 3,
                    Code = "waiting_for_shipment",
                    Name = "Oczekuje na realizację"
                },
                 new OrderStatus()
                {
                    Id = 4,
                    Code = "send",
                    Name = "Wysłane"
                },
                new OrderStatus()
                {
                    Id = 5,
                    Code = "ready",
                    Name = "Gotowe do odbioru"
                },
                new OrderStatus()
                {
                    Id = 6,
                    Code = "finished",
                    Name = "Zrealizowane"
                }
            };

        }

    }
}
