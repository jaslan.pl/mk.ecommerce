﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [Serializable]
    public sealed class Orders : Dictionary<string, Order>, IHasResponseCode
    {
        public Orders()
        {
        }

        private Orders(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        [DataMember(Name = "response_code")]
        public int ResponseCode { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
