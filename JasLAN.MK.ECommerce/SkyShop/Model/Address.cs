﻿using JasLAN.MK.ECommerce.Generic.Model;
using System.Runtime.Serialization;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [DataContract]
    public class Address : IAddress
    {
        public string City { get; set; }

        public string Company { get; set; }

        public string Country { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string TaxIdentificationNumber { get; set; }

        public string Phone { get; set; }

        public string Postcode { get; set; }

        public string Street { get; set; }
    }
}
