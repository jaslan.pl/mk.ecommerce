﻿using JasLAN.MK.ECommerce.Generic.Model;
using System.Runtime.Serialization;

namespace JasLAN.MK.ECommerce.SkyShop.Model
{
    [DataContract]
    public class InvoiceProduct : IInvoiceProduct
    {
        [DataMember(Name = "amount")]
        public string Amount { get; set; }

        [DataMember(Name = "price_net")]
        public decimal Price { get; set; }

        [DataMember(Name = "price")]
        public decimal PriceGross { get; set; }
    
        public decimal Quantity
        {
            get
            {
                decimal.TryParse(this.Amount, out decimal quantity);
                return quantity;
            }
        }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "prod_symbol")]
        public string Code { get; set; }

        [DataMember(Name = "tax")]
        public string Tax { get; set; }

        public decimal TaxValue
        {
            get
            {
                return PriceGross - Price;
            }
        }

        [DataMember(Name = "unit")]
        public string Unit { get; set; }

        public decimal Discount
        {
            get
            {
                return 0;
            }
        }
    }
}
