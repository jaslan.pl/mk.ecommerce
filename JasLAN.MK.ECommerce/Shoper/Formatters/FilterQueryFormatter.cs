﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System.Linq;
using System.Web;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    public class FilterQueryFormatter : Formatter<FilterQuery>
    {
        public override string ToQuery(FilterQuery filterQuery)
        {
            if (!filterQuery.Filters.Any())
            {
                return null;
            }
            var filterFormatter = new FilterFormatter();

            var query = filterQuery.Filters.Select(o => filterFormatter.ToQuery(o))
                                           .Aggregate((o, p) => o += "," + p);

            return "filters=" + HttpUtility.UrlEncode("{" + query + "}");
        }
    }
}
