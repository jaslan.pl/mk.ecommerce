﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System.Text;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    public class PageQueryFormatter : Formatter<PageQuery>
    {
        public override string ToQuery(PageQuery subject)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"page={subject.PageNumber}&limit={subject.PageSize}");
            foreach (string order in subject.Order)
            {
                sb.Append($"&order={order}");
                break;
            }

            return sb.ToString();
        }
    }
}
