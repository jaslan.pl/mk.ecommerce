﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    public class FilterFormatter : Formatter<Filter>
    {
        public override string ToQuery(Filter filter)
        {
            string value = Convert.ToString(filter.Value);

            if (filter.Value != null && filter.Value.GetType() == typeof(string))
            {
                value = $"\"{value}\"";
            }

            if (filter.Operator == "=")
            {
                return $"\"{filter.Field}\":{value}";
            }
            else
            {
                return $"\"{filter.Field}\":{{\"{filter.Operator}\":{value}}}";
            }
        }
    }
}
