﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class OptionValueTranslation
    {
        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
