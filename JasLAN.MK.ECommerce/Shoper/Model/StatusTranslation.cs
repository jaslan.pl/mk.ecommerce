﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class StatusTranslation
    {
        /// <summary>
        /// Translation identifier
        /// </summary>
        [DataMember(Name = "trans_id")]
        public int Id { get; set; }

        /// <summary>
        /// Status identifier
        /// </summary>
        [DataMember(Name = "status_id")]
        public int StatusId { get; set; }

        /// <summary>
        /// Status name
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Language identifier
        /// </summary>
        [DataMember(Name = "lang_id")]
        public int LanguageId { get; set; }


        /// <summary>
        /// e-mail contents in TXT format
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }

        /// <summary>
        /// e-mail contents in HTML format
        /// </summary>
        [DataMember(Name = "message_html")]
        public string MessageHtml { get; set; }
    }
}
