﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Address : IAddress
    {
        [DataMember(Name = "address_id")]
        public int AddressId { get; set; }

        [DataMember(Name = "order_id")]
        public int OrderId { get; set; }

        [DataMember(Name = "type")]
        public int Type { get; set; }

        [DataMember(Name = "firstname")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastname")]
        public string LastName { get; set; }

        [DataMember(Name = "company")]
        public string Company { get; set; }

        [DataMember(Name = "tax_identification_number")]
        public string TaxIdentificationNumber { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "postcode")]
        public string Postcode { get; set; }

        [DataMember(Name = "street1")]
        public string Street1 { get; set; }

        [DataMember(Name = "street2")]
        public string Street2 { get; set; }

        public string Street
        {
            get
            {
                return $"{this.Street1} {this.Street2}".Trim();
            }
        }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "country_code")]
        public string CountryCode { get; set; }
    }
}
