﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Translation
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "active")]
        public int Active { get; set; }
    }
}
