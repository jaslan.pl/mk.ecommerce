﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class ProductStock
    {
        [DataMember(Name = "stock_id")]
        public int StockId { get; set; }

        [DataMember(Name = "product_id")]
        public int ProductId { get; set; }

        [DataMember(Name = "stock")]
        public decimal Stock { get; set; }

        [DataMember(Name = "price")]
        public decimal Price { get; set; }

        [DataMember(Name = "comp_promo_price")]
        public decimal CompPromoPrice { get; set; }

        [DataMember(Name = "price_type")]
        public int PriceType { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "ean")]
        public string Ean { get; set; }

        [DataMember(Name = "active")]
        public int Active { get; set; }

        [DataMember(Name = "options")]
        public IDictionary<string, string> Options { get; set; }
    }
}
