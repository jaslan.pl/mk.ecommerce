﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Auction
    {
        [DataMember(Name = "auction_order_id")]
        public long AuctionOrderId { get; set; }

        [DataMember(Name = "auction_id")]
        public long? AuctionId { get; set; }

        [DataMember(Name = "real_auction_id")]
        public long? RealAuctionId { get; set; }

        [DataMember(Name = "order_id")]
        public int? OrderId { get; set; }

        [DataMember(Name = "buyer_id")]
        public int? BuyerId { get; set; }

        [DataMember(Name = "status_time")]
        public string StatusTime { get; set; }

        [DataMember(Name = "payment_time")]
        public string PaymentTime { get; set; }

        [DataMember(Name = "payment_method")]
        public string PaymentMethod { get; set; }

        [DataMember(Name = "shipment_method")]
        public string ShipmentMethod { get; set; }

        [DataMember(Name = "transaction_id")]
        public int? TransactionId { get; set; }

        [DataMember(Name = "buyer_login")]
        public string BuyerLogin { get; set; }
    }
}
