﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class PageResponse<T> : IPageResponse<T>
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }

        [DataMember(Name = "pages")]
        public int Pages { get; set; }

        [DataMember(Name = "page")]
        public int Page { get; set; }

        [DataMember(Name = "list")]
        public T[] List { get; set; }    
    }
}
