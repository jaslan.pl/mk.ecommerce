﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Product : IProduct
    {
        [DataMember(Name = "product_id")]
        public int ProductId { get; set; }

        [OrderBy("stock.stock_id")]
        public int StockId
        {
            get
            {
                if (ProductStock == null)
                {
                    return 0;
                }

                return ProductStock.StockId;
            }
        }

        [DataMember(Name = "code")]
        [OrderBy("stock.code")]
        public string Code { get; set; }

        [DataMember(Name = "ean")]
        [OrderBy("stock.ean")]
        public string Ean { get; set; }

        [OrderBy("stock.stock")]
        public decimal Stock
        {
            get
            {
                if (ProductStock == null)
                {
                    return 0;
                }

                return ProductStock.Stock;
            }
        }

        [OrderBy("stock.price")]
        public decimal Price
        {
            get
            {
                if (ProductStock == null)
                {
                    return 0;
                }

                return ProductStock.Price;
            }
        }

        [OrderBy("stock.comp_promo_price")]
        public decimal Price2
        {
            get
            {
                if (ProductStock == null)
                {
                    return 0;
                }

                return ProductStock.CompPromoPrice;
            }
        }
      
        [DataMember(Name = "stock")]
        public ProductStock ProductStock { get; set; }

        [DataMember(Name = "translations")]
        public IDictionary<string, Translation> Translations { get; set; }

        [OrderBy("translations.pl_PL.name")]
        public string Name
        {
            get
            {
                if (Translations == null)
                {
                    return null;
                }

                if (!Translations.ContainsKey("pl_PL"))
                {
                    if (Translations.Any())
                    {
                        return Translations.First().Value.Name;
                    }

                    return null;
                }

                return Translations["pl_PL"].Name;
            }
        }

        [OrderBy("translations.pl_PL.active")]
        public bool IsActive
        {
            get
            {
                if (Translations == null)
                {
                    return false;
                }

                if (!Translations.ContainsKey("pl_PL"))
                {
                    return false;
                }

                return Translations["pl_PL"].Active != 0;
            }
        }

        [DataMember(Name = "options")]
        public ICollection<int> Options { get; set; }
    }
}
