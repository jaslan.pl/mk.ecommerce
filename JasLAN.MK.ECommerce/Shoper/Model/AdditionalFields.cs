﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class AdditionalFields
    {
        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "field_id")]
        public int FieldId { get; set; }

        [DataMember(Name = "type")]
        public int Type { get; set; }

        [DataMember(Name = "locate")]
        public int Locate { get; set; }

        [DataMember(Name = "req")]
        public bool Req { get; set; }

        [DataMember(Name = "active")]
        public bool Active { get; set; }

        [DataMember(Name = "order")]
        public int Order { get; set; }
    }
}
