﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class OrderProduct : IOrderProduct
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "order_id")]
        public int OrderId { get; set; }

        [DataMember(Name = "product_id")]
        public int ProductId { get; set; }

        [DataMember(Name = "stock_id")]
        public int StockId { get; set; }

        [DataMember(Name = "price")]
        public decimal Price { get; set; }

        [DataMember(Name = "discount_perc")]
        public decimal Discount { get; set; }

        [DataMember(Name = "quantity")]
        public decimal Quantity { get; set; }

        [DataMember(Name = "delivery_time")]
        public string DeliveryTime { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "pkwiu")]
        public string PKWiU { get; set; }

        [DataMember(Name = "tax")]
        public string Tax { get; set; }

        [DataMember(Name = "tax_value")]
        public decimal TaxValue { get; set; }

        [DataMember(Name = "unit")]
        public string Unit { get; set; }

        [DataMember(Name = "option")]
        public string Option { get; set; }

        [DataMember(Name = "unit_fp")]
        public bool UnitFp { get; set; }

        [DataMember(Name = "weight")]
        public float Weight { get; set; }

        [DataMember(Name = "text_options")]
        public ICollection<TextOption> TextOptions { get; set; }

        [DataMember(Name = "file_options")]
        public ICollection<FileOption> FileOptions { get; set; }        
    }
}
