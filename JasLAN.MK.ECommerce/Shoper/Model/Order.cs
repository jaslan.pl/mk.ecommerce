﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Order : IOrder
    {
        [DataMember(Name = "order_id")]
        public int OrderId { get; set; }

        [DataMember(Name = "user_id")]
        public int? UserId { get; set; }

        [DataMember(Name = "date")]
        public string Date { get; set; }

        [DataMember(Name = "status_date")]
        public string StatusDate { get; set; }

        [DataMember(Name = "confirm_date")]
        public string ConfirmDate { get; set; }

        [DataMember(Name = "delivery_date")]
        public string DeliveryDate { get; set; }

        [DataMember(Name = "status_id")]
        public int OrderStatusId { get; set; }

        [DataMember(Name = "status")]
        public Status Status { get; set; }

        [DataMember(Name = "sum")]
        public decimal Total { get; set; }
       
        [DataMember(Name = "payment_id")]
        public int? PaymentId { get; set; }

        [DataMember(Name = "user_order")]
        public int? UserOrder { get; set; }

        [DataMember(Name = "shipping_id")]
        public int? ShippingId { get; set; }

        [DataMember(Name = "shipping_cost")]
        public decimal TotalShipping { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "delivery_code")]
        public string DeliveryCode { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "confirm")]
        public bool Confirm { get; set; }

        [DataMember(Name = "notes")]
        public string Notes { get; set; }

        [DataMember(Name = "notes_priv")]
        public string NotesPriv { get; set; }

        [DataMember(Name = "notes_pub")]
        public string NotesPub { get; set; }

        [DataMember(Name = "currency_id")]
        public int? CurrencyId { get; set; }

        [DataMember(Name = "currency_rate")]
        public float CurrencyRate { get; set; }

        [DataMember(Name = "paid")]
        public decimal TotalPaid { get; set; }

        [DataMember(Name = "ip_address")]
        public string IpAddress { get; set; }

        [DataMember(Name = "discount_client")]
        public float DiscountClient { get; set; }

        [DataMember(Name = "discount_group")]
        public float DiscountGroup { get; set; }

        [DataMember(Name = "discount_levels")]
        public float DiscountLevels { get; set; }

        [DataMember(Name = "discount_code")]
        public float DiscountCode { get; set; }

        [DataMember(Name = "shipping_tax_id")]
        public int? ShippingTaxId { get; set; }

        [DataMember(Name = "shipping_tax_value")]
        public float ShippingTaxValue { get; set; }

        [DataMember(Name = "shipping_tax_name")]
        public string ShippingTaxName { get; set; }

        [DataMember(Name = "code_id")]
        public int? CodeId { get; set; }

        [DataMember(Name = "lang_id")]
        public int? LangId { get; set; }

        [DataMember(Name = "origin")]
        public int? Origin { get; set; }

        [DataMember(Name = "promo_code")]
        public string PromoCode { get; set; }

        [DataMember(Name = "is_paid")]
        public bool IsPaid { get; set; }

        [DataMember(Name = "is_underpayment")]
        public bool IsUnderpayment { get; set; }

        [DataMember(Name = "is_overpayment")]
        public bool IsOverpayment { get; set; }

        [DataMember(Name = "total_products")]
        public int ProductsCount { get; set; }

        [DataMember(Name = "total_parcels")]
        public int TotalParcels { get; set; }

        [DataMember(Name = "billing_address")]
        public Address BillingAddress { get; set; }

        [DataMember(Name = "delivery_address")]
        public Address DeliveryAddress { get; set; }

        [DataMember(Name = "auction")]
        public Auction Auction { get; set; }

        [DataMember(Name = "additional_fields")]
        public ICollection<AdditionalFields> AdditionalFields { get; set; }

        IAddress IOrder.BillingAddress => this.BillingAddress;

        IAddress IOrder.DeliveryAddress => this.DeliveryAddress;
    }
}
