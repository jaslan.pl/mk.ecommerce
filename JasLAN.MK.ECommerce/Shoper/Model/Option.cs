﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Option
    {
        [DataMember(Name = "option_id")]
        public int OptionId { get; set; }

        [DataMember(Name = "translations")]
        public IDictionary<string, Translation> Translations { get; set; }

        [OrderBy("translations.pl_PL.name")]
        public string Name
        {
            get
            {
                if (Translations == null)
                {
                    return null;
                }

                if (!Translations.ContainsKey("pl_PL"))
                {
                    if (Translations.Any())
                    {
                        return Translations.First().Value.Name;
                    }

                    return null;
                }

                return Translations["pl_PL"].Name;
            }
        }
    }
}
