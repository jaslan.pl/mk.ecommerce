﻿using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Shoper.Model
{
    [DataContract]
    public class Status : IOrderStatus
    {
        [DataMember(Name ="status_id")]
        public int Id { get; set; }

        [DataMember(Name = "active")]
        public int Active { get; set; }

        [DataMember(Name = "default")]
        public int Default { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }

        /// <summary>
        /// 1 - new,
        /// 2 - opened,
        /// 3 - closed,
        /// 4 - not completed
        /// </summary>
        [DataMember(Name = "type")]
        public int Type { get; set; }

        [DataMember(Name = "email_change")]
        public bool EmailChange { get; set; }

        [DataMember(Name = "order")]
        public int Order { get; set; }

        /// <summary>
        /// Array an associative array with object translations
        /// </summary>
        [DataMember(Name = "translations")]
        public Dictionary<string, StatusTranslation> Translations { get; set; }

        /// <summary>
        /// Gets polish name
        /// </summary>
        public string Name
        {
            get
            {
                return this.Translations.Where(o => o.Key == "pl_PL")
                                        .Select(o => o.Value)
                                        .Select(o => o.Name)
                                        .FirstOrDefault() ?? string.Empty;
            }
        }
    }
}
