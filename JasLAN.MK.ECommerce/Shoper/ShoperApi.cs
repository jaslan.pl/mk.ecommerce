﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using JasLAN.MK.ECommerce.Generic;
using JasLAN.MK.ECommerce.Shoper.Model;
using JasLAN.MK.ECommerce.Generic.Model;
using JasLAN.MK.ECommerce.Generic.Formatters;
using System.Collections.ObjectModel;
using System.Threading;

namespace JasLAN.MK.ECommerce.Shoper
{
    /// <summary>
    /// https://developers.shoper.pl/developers/api/
    /// </summary>
    public class ShoperApi : IApi
    {
        private AuthResponse authResponse;
        private string url;

        private readonly IDictionary<int, Option> options;
        private readonly IDictionary<int, OptionValue> optionValues;

        public ShoperApi()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            this.options = new Dictionary<int, Option>();
            this.optionValues = new Dictionary<int, OptionValue>();
        }

        public string Url
        {
            get
            {
                return this.url;
            }

            set
            {
                this.url = value;

                if (!string.IsNullOrEmpty(this.url))
                {
                    if (!this.url.ToLower().StartsWith("https://") &&
                        !this.url.ToLower().StartsWith("http://"))
                    {
                        this.url = $"https://{this.url}";
                    }
                }
            }
        }

        public FormattersCollection Formatters
        {
            get
            {
                return new FormattersCollection(
                    new Dictionary<Type, Func<IFormatter>> {
                        { typeof(Filter), () => new FilterFormatter() },
                        { typeof(FilterQuery), () => new FilterQueryFormatter() },
                        { typeof(PageQuery), () => new PageQueryFormatter() } }
                );
            }
        }

        /// <summary>
        /// Returns AuthResponse or null if not authorized.
        /// Requires setting <see cref="Url"/> before.
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="password">Password</param>
        /// <returns>AuthResponse or null if not authorized</returns>
        [DebuggerStepThrough]
        public bool Login(string login, string password)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new ArgumentNullException(nameof(login));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (Url == null)
            {
                throw new InvalidOperationException($"{nameof(Url)} not set");
            }

            this.authResponse = null;

            WebRequest request = WebRequest.Create($"{Url}/webapi/rest/auth");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Basic " + $"{login}:{password}".Base64Encode());

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    Throttle(response);

                    this.authResponse = response.ReadObject<AuthResponse>();

                    return true;
                }
            }
            catch (WebException ex)
            {
                if (ex.Response != null && (ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return false;
                }

                throw;
            }
        }

        /// <summary>
        /// Fetches objects collection
        /// </summary>
        public IPageResponse<IOrder> GetOrders(PageQuery pageQuery, FilterQuery filterQuery, int orderStatusId)
        {
            //limit integer count of fetched objects, default: 10, max: 50
            //order   string collection sort field
            //page integer index of requested page, default: 1
            //offset integer starting record index(used instead of page)
            //filters string JSON-ified string with filtering criteria

            if (orderStatusId != 0)
            {
                filterQuery.Filters.Add(new Filter("status_id", orderStatusId));
            }

            pageQuery.MapToApiProperties<Order>();

            WebRequest request = this.Get(this.MakeQuery($"orders", pageQuery, filterQuery));
            using (WebResponse response = request.GetResponse())
            {
                Throttle(response);

                var pageResponse = response.ReadObject<PageResponse<Order>>();

                return new PageResponse<IOrder>()
                {
                    Count = pageResponse.Count,
                    List = pageResponse.List.AsEnumerable<IOrder>().ToArray(),
                    Page = pageResponse.Page,
                    Pages = pageResponse.Pages
                };
            }
        }

        /// <summary>
        /// Fetches objects collection
        /// </summary>
        public IPageResponse<IOrderProduct> GetOrderProducts(int orderId, PageQuery pageQuery)
        {           
            var filterQuery = new FilterQuery();
            filterQuery.Filters.Add(new Filter("order_id", orderId));

            string query = this.MakeQuery($"order-products", pageQuery, filterQuery);

            WebRequest request = this.Get(query);
            using (WebResponse response = request.GetResponse())
            {
                Throttle(response);

                var pageResponse = response.ReadObject<PageResponse<OrderProduct>>();

                return new PageResponse<IOrderProduct>()
                {
                    Count = pageResponse.Count,
                    List = pageResponse.List.AsEnumerable<IOrderProduct>().ToArray(),
                    Page = pageResponse.Page,
                    Pages = pageResponse.Pages
                };
            }
        }

        public IPageResponse<IOrderStatus> GetOrderStatuses(PageQuery pageQuery, FilterQuery filterQuery)
        {
            //limit integer count of fetched objects, default: 10, max: 50
            //order   string collection sort field
            //page integer index of requested page, default: 1
            //offset integer starting record index(used instead of page)
            //filters string JSON-ified string with filtering criteria

            filterQuery.Filters.Add(new Filter("active", "1"));
            pageQuery.Order.Add("order");

            WebRequest request = this.Get(this.MakeQuery($"statuses", pageQuery, filterQuery));
            using (WebResponse response = request.GetResponse())
            {
                Throttle(response);

                var pageResponse = response.ReadObject<PageResponse<Status>>();

                return new PageResponse<IOrderStatus>()
                {
                    Count = pageResponse.Count,
                    List = pageResponse.List.AsEnumerable<IOrderStatus>().ToArray(),
                    Page = pageResponse.Page,
                    Pages = pageResponse.Pages
                };
            }
        }

        private WebRequest Get(string resource)
        {
            Debug.WriteLine(resource);

            if (Url == null)
            {
                throw new InvalidOperationException("ShopUrl not set");
            }

            if (authResponse == null)
            {
                throw new InvalidOperationException("Not authenticated");
            }

            WebRequest request = WebRequest.Create($"{Url}/webapi/rest/{resource}");
            request.Method = "GET";
            request.Headers.Add("Authorization", "Bearer " + authResponse.AccessToken);

            return request;
        }

        private WebRequest Post(string resource)
        {
            if (Url == null)
            {
                throw new InvalidOperationException("ShopUrl not set");
            }

            if (authResponse == null)
            {
                throw new InvalidOperationException("Not authenticated");
            }

            WebRequest request = WebRequest.Create($"{Url}/webapi/rest/{resource}");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Bearer " + authResponse.AccessToken);

            return request;
        }

        private WebRequest Put(string resource, object payload)
        {
            if (Url == null)
            {
                throw new InvalidOperationException("ShopUrl not set");
            }

            if (authResponse == null)
            {
                throw new InvalidOperationException("Not authenticated");
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{Url}/webapi/rest/{resource}");
            request.Method = "PUT";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Bearer " + authResponse.AccessToken);
            if (payload != null)
            {
                string data = payload.WriteObject();
                byte[] bytes = Encoding.UTF8.GetBytes(data);
                request.ContentLength = bytes.Length;

                using (Stream sendStream = request.GetRequestStream())
                {
                    sendStream.Write(bytes, 0, bytes.Length);
                    sendStream.Close();
                }
            }

            return request;
        }

        public IPageResponse<IInvoice> GetInvoices(PageQuery pageQuery, FilterQuery filterQuery)
        {
            throw new NotImplementedException();
        }

        public IPageResponse<IProduct> GetProducts(bool onlyActive, PageQuery pageQuery, FilterQuery filterQuery)
        {
            if (onlyActive)
            {
                filterQuery.Filters.Add(new Filter("translations.pl_PL.active", 1));
            }

            return GetProducts(pageQuery, filterQuery);
        }

        private IPageResponse<IProduct> GetProducts(PageQuery pageQuery, FilterQuery filterQuery)
        {          
            pageQuery.MapToApiProperties<Product>();
            filterQuery.MapToApiProperties<Product>();

            WebRequest request = this.Get(this.MakeQuery($"products", pageQuery, filterQuery));
            using (WebResponse response = request.GetResponse())
            {
                Throttle(response);

                var pageResponse = response.ReadObject<PageResponse<Product>>();

                pageResponse.List = GetVariants(pageResponse.List).ToArray();

                return new PageResponse<IProduct>()
                {
                    Count = pageResponse.Count,
                    List = pageResponse.List.AsEnumerable<IProduct>().ToArray(),
                    Page = pageResponse.Page,
                    Pages = pageResponse.Pages
                };
            }
        }

        public IProduct GetProductByCode(string code)
        {
            var products = this.GetProducts(new PageQuery(), new FilterQuery()
            {
                Filters =
                {
                    new Filter("stock.code", code)
                }
            });

            return products.List.FirstOrDefault();
        }

        public IProduct GetProductByEan(string ean)
        {
            var products = this.GetProducts(new PageQuery(), new FilterQuery()
            {
                Filters =
                {
                    new Filter("stock.ean", ean)
                }
            });

            return products.List.FirstOrDefault();
        }

        public IProduct GetProductByProductId(int productId)
        {
            var products = this.GetProducts(new PageQuery(), new FilterQuery()
            {
                Filters =
                {
                    new Filter("product_id", productId)
                }
            });

            return products.List.FirstOrDefault();
        }

        public bool UpdateProductStock(int productId, int stockId, decimal stock)
        {
            if (stockId != 0)
            {
                var productStock = GetProductStock(stockId);
                return UpdateProductStock(productStock, stock);
            }

            var product = (Product)this.GetProductByProductId(productId);

            if (product == null)
            {
                return false;
            }

            return UpdateProductStock(product.ProductStock, stock);
        }

        private ICollection<Product> GetVariants(ICollection<Product> products)
        {
            var result = new Collection<Product>();

            foreach (var product in products)
            {
                if (result.Any(o => o.ProductId == product.ProductId))
                {
                    // Already added - usually a variant with the same name
                    continue;
                }

                result.Add(product);

                if (product.Options.Any())
                {
                    foreach (var productStockId in product.Options)
                    {
                        var productStock = GetProductStock(productStockId);

                        string name = product.Name;

                        name += " (";

                        foreach (var optionItem in productStock.Options)
                        {
                            var option = GetOption(int.Parse(optionItem.Key));
                            var optionValue = GetOptionValue(int.Parse(optionItem.Value));

                            name += option.Name + ": " + optionValue.Name;
                        }

                        name += ")";
                       
                        if (productStock.Price == 0)
                        {
                            productStock.Price = product.ProductStock.Price;
                        }

                        productStock.CompPromoPrice = product.ProductStock.CompPromoPrice;
                        
                        var variant = new Product()
                        {
                            Code = productStock.Code,
                            Ean = productStock.Ean,
                            ProductId = productStock.ProductId,
                            ProductStock = productStock,
                            Translations = new Dictionary<string, Translation>()
                            {
                                { "pl_PL",
                                    new Translation()
                                    {
                                        Active = productStock.Active,
                                        Name = name
                                    }
                                }
                            }
                        };

                        result.Add(variant);
                    }
                }
            }

            return result;
        }

        private Option GetOption(int optionId)
        {
            return Get("options", optionId, options);
        }

        private OptionValue GetOptionValue(int optionValueId)
        {
            return Get("option-values", optionValueId, optionValues);
        }

        private T Get<T>(string resource, int optionId, IDictionary<int, T> cache)
        {
            if (cache.ContainsKey(optionId))
            {
                return cache[optionId];
            }

            WebRequest request = this.Get($"{resource}/{optionId}");
            using (WebResponse response = request.GetResponse())
            {
                Throttle(response);

                var @object = response.ReadObject<T>();

                cache.Add(optionId, @object);

                return @object;
            }
        }

        private ProductStock GetProductStock(int productStockId)
        {
            WebRequest request = this.Get($"product-stocks/{productStockId}");
            using (WebResponse response = request.GetResponse())
            {
                Throttle(response);

                return response.ReadObject<ProductStock>();
            }
        }

        private bool UpdateProductStock(ProductStock productStock, decimal stock)
        {
            if (productStock.Stock == stock)
            {
                return true;
            }

            productStock.Stock = stock;
            WebRequest request = this.Put(this.MakeQuery($"product-stocks/{productStock.StockId}"), productStock);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Throttle(response);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }

                return false;
            }
        }

        private void Throttle(WebResponse response)
        {
            int apiLimit = int.Parse(response.Headers["X-Shop-Api-Limit"]);
            int apiCalls = int.Parse(response.Headers["X-Shop-Api-Calls"]);

            //Debug.WriteLine($"Throttling {apiCalls}/{apiLimit}");
            if (apiCalls >= apiLimit)
            {
                Thread.Sleep(1000);
            }
        }
    }
}
