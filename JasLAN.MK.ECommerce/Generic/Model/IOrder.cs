﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Order object
    /// </summary>
    public interface IOrder
    {
        /// <summary>
        /// Gets order identifier
        /// </summary>
        int OrderId { get; }

        /// <summary>
        /// Gets order date
        /// </summary>
        string Date { get; }

        /// <summary>
        /// Gets order delivery date
        /// </summary>
        string DeliveryDate { get; }

        /// <summary>
        /// Gets order status identifier <see cref="IOrderStatus"/>
        /// </summary>
        int OrderStatusId { get; }

        /// <summary>
        /// Gets email address
        /// </summary>
        string Email { get; }

        /// <summary>
        /// Gets notes
        /// </summary>
        string Notes { get; }

        /// <summary>
        /// Gets order total value
        /// </summary>
        decimal Total { get; }

        /// <summary>
        /// Gets order total paid value
        /// </summary>
        decimal TotalPaid { get; }

        /// <summary>
        /// Gets order total shipping costs
        /// </summary>
        decimal TotalShipping { get; }

        /// <summary>
        /// Gets total order products count
        /// </summary>
        int ProductsCount { get; }

        /// <summary>
        /// Gets billing address
        /// </summary>
        IAddress BillingAddress { get; }

        /// <summary>
        /// Gets delivery address
        /// </summary>
        IAddress DeliveryAddress { get; }
    }
}
