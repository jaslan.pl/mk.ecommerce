﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Page response object
    /// </summary>
    /// <typeparam name="T">Type of objects in page</typeparam>
    public interface IPageResponse<T>
    {
        /// <summary>
        /// Gets or sets count of all objects in all pages
        /// </summary>
        int Count { get; set; }

        /// <summary>
        /// Gets or sets count of all pages
        /// </summary>
        int Pages { get; set; }

        /// <summary>
        /// Gets or sets current page
        /// </summary>
        int Page { get; set; }

        /// <summary>
        /// Gets or sets current page list
        /// </summary>
        T[] List { get; set; }
    }
}
