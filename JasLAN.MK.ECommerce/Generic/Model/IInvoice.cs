﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Invoice object
    /// </summary>
    public interface IInvoice
    {
        /// <summary>
        /// Gets order identifier
        /// </summary>
        int InvoiceId { get; }

        /// <summary>
        /// Gets invoice number
        /// </summary>
        string Number { get; }

        /// <summary>
        /// Gets invoice date
        /// </summary>
        string Date { get; }

        /// <summary>
        /// Gets invoice delivery date
        /// </summary>
        string SaleDate { get; }
      
        /// <summary>
        /// Gets order total value
        /// </summary>
        decimal Total { get; }

        /// <summary>
        /// Gets order total value
        /// </summary>
        string PaymentForm { get; }

        /// <summary>
        /// Gets billing address
        /// </summary>
        IAddress BillingAddress { get; }

        decimal Net23 { get; }

        decimal Vat23 { get; }

        decimal Net8 { get; }

        decimal Vat8 { get; }
    }
}
