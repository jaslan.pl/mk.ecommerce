﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Filter definition
    /// </summary>
    [DebuggerDisplay("{Field} {Operator} {Value}")]
    [DebuggerStepThrough]
    public class Filter : IUrlFormattable
    {
        /// <summary>
        /// Initializes a new instance of filter with any operator
        /// </summary>
        /// <param name="field">Filed to be filtered</param>
        /// <param name="operator">Operator to be applied</param>
        /// <param name="value">Value of filter</param>
        public Filter(string field, string @operator, object value)
        {
            this.Field = field;
            this.Operator = @operator;
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of filter with equality operator
        /// </summary>
        /// <param name="field">Field to filter</param>
        /// <param name="value">Value of filter</param>
        public Filter(string field, object value)
            : this(field, "=", value)
        {
        }

        /// <summary>
        /// Filter field
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// Filter operator
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// Filter value
        /// </summary>
        public object Value { get; set; }
    }
}
