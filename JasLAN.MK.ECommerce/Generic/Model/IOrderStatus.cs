﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Order status
    /// </summary>
    public interface IOrderStatus
    {
        /// <summary>
        /// Gets order status identife
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Gets order status name
        /// </summary>
        string Name { get; }
    }
}
