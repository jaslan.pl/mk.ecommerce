﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Filter query
    /// </summary>
    [DebuggerStepThrough]
    public class FilterQuery : IUrlFormattable
    {
        /// <summary>
        /// Initializes a new filter query
        /// </summary>
        public FilterQuery()
        {
            this.Filters = new Collection<Filter>();
        }

        /// <summary>
        /// Gets collection of filters
        /// </summary>
        public ICollection<Filter> Filters { get; private set; }

        /// <summary>
        /// Map <see cref="Order"/> properties to XML representation
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void MapToApiProperties<T>()
        {
            this.Filters = this.Filters.MapToApiProperties<T>();
        }
    }
}
