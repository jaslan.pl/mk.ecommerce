﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    public interface IProduct
    {
        int ProductId { get; }

        int StockId { get; }

        bool IsActive { get; }

        string Name { get; }

        string Code { get; }

        string Ean { get; }

        decimal Stock { get; }

        decimal Price { get; }

        decimal Price2 { get; }
    }
}
