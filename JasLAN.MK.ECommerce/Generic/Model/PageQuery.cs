﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Page query object
    /// </summary>
    [DebuggerStepThrough]
    public class PageQuery : IUrlFormattable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PageQuery"/>
        /// </summary>
        public PageQuery()
        {
            this.PageNumber = 1;
            this.PageSize = 50;
            this.Order = new Collection<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageQuery"/>
        /// </summary>
        /// <param name="pageNumer">Page number</param>
        public PageQuery(int pageNumer)
            : this()
        {
            this.PageNumber = pageNumer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageQuery"/>
        /// </summary>
        /// <param name="pageNumer">Page number</param>
        /// <param name="pageSize">Page size</param>
        public PageQuery(int pageNumer, int pageSize)
          : this()
        {
            this.PageNumber = pageNumer;
            this.PageSize = pageSize;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageQuery"/>
        /// </summary>
        /// /// <param name="pageNumer">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortOrder"></param>
        public PageQuery(int pageNumer, int pageSize, string sortOrder)
          : this()
        {
            this.PageNumber = pageNumer;
            this.PageSize = pageSize;
            if (!string.IsNullOrEmpty(sortOrder))
            {
                this.Order.Add(sortOrder);
            }
        }

        public int Start
        {
            get
            {
                return (this.PageNumber - 1) * this.PageSize;
            }
        }

        /// <summary>
        /// Gets or sets page number
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets page size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets sort order
        /// </summary>
        public ICollection<string> Order { get; private set; }

        /// <summary>
        /// Map <see cref="Order"/> properties to XML representation
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void MapToApiProperties<T>()
        {
            this.Order = this.Order.MapToApiProperties<T>()
                                   .Where(o => o != "date_add")
                                   .ToList();
        }
    }
}
