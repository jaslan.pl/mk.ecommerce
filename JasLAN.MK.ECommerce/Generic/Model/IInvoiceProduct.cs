﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Invoice product
    /// </summary>
    public interface IInvoiceProduct
    {
        /// <summary>
        /// Gets product unit price
        /// </summary>
        decimal Price { get; }

        /// <summary>
        /// Discount percentage
        /// </summary>
        decimal Discount { get; }

        /// <summary>
        /// Gets product count
        /// </summary>
        decimal Quantity { get; }

        /// <summary>
        /// Gets product name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets product internal code
        /// </summary>
        string Code { get; }
        
        /// <summary>
        /// Gets product tax rate
        /// </summary>
        string Tax { get; }

        /// <summary>
        /// Gets product tax value
        /// </summary>
        decimal TaxValue { get; }

        /// <summary>
        /// Gets product unit of measure
        /// </summary>
        string Unit { get; }
    }
}
