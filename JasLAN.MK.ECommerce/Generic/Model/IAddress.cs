﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Model
{
    /// <summary>
    /// Order address
    /// </summary>
    public interface IAddress
    {
        /// <summary>
        /// Gets city
        /// </summary>
        string City { get; }

        /// <summary>
        /// Gets company name
        /// </summary>
        string Company { get; }

        /// <summary>
        /// Gets country
        /// </summary>
        string Country { get; }

        /// <summary>
        /// Gets first name
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Gets last name
        /// </summary>
        string LastName { get; }

        /// <summary>
        /// Gets tax identification numer (NIP)
        /// </summary>
        string TaxIdentificationNumber { get; }

        /// <summary>
        /// Get phone numer
        /// </summary>
        string Phone { get; }

        /// <summary>
        /// Get post code
        /// </summary>
        string Postcode { get; }

        /// <summary>
        /// Get street address
        /// </summary>
        string Street { get; }
    }
}
