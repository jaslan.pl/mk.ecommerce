﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;

namespace JasLAN.MK.ECommerce.Generic
{
    /// <summary>
    /// Extension class
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Serializes object as JSON string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string WriteObject<T>(this T input)
        {
            var settings = new DataContractJsonSerializerSettings()
            {
                UseSimpleDictionaryFormat = true
            };

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(input.GetType(), settings);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.WriteObject(memoryStream, input);

                memoryStream.Seek(0, SeekOrigin.Begin);

                using (var streamReader = new StreamReader(memoryStream))
                {
                    string result = streamReader.ReadToEnd();
                    return result;
                }
            }
        }

        /// <summary>
        /// Read object from string implementation
        /// </summary>
        /// <typeparam name="T">Type of object to read</typeparam>
        /// <param name="input">Input string</param>
        /// <returns>Object read</returns>
        public static T ReadObject<T>(this string input)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(input)))
            {
                return ReadObject<T>(memoryStream);
            }
        }

        /// <summary>
        /// Read object from stream
        /// </summary>
        /// <typeparam name="T">Type of object to read</typeparam>
        /// <param name="stream">Stream to read from</param>
        /// <returns>Object read</returns>
        public static T ReadObject<T>(this Stream stream)
        {
            var settings = new DataContractJsonSerializerSettings()
            {
                UseSimpleDictionaryFormat = true
            };

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), settings);
            return (T)serializer.ReadObject(stream);
        }

        /// <summary>
        /// Read object from <paramref name="response"/>
        /// </summary>
        /// <typeparam name="T">Type of object to read</typeparam>
        /// <param name="response">Response to read from</param>
        /// <returns>Object read</returns>
        public static T ReadObject<T>(this WebResponse response)
        {
#if DEBUG
            string stringResponse = response.ReadString();

            return ReadObject<T>(stringResponse);
#else
            return response.GetResponseStream().ReadObject<T>();
#endif
        }

        /// <summary>
        /// Read string from response
        /// </summary>
        /// <param name="response">Respone to read from</param>
        /// <returns>String from response</returns>
        public static string ReadString(this WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream);
                string text = reader.ReadToEnd();
                return text;
            }
        }

        /// <summary>
        /// Encode string in B64
        /// </summary>
        /// <param name="plainText">Text to encode</param>
        /// <returns>B64 representation of string</returns>
        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// Make URL for <paramref name="resource"/>> and query parameters from formattale queries using formatters from <paramref name="api"/>
        /// </summary>
        /// <param name="api">API used</param>
        /// <param name="resource">Base URL resource</param>
        /// <param name="queries"></param>
        /// <returns></returns>
        public static string MakeQuery(this IApi api, string resource, params IUrlFormattable[] queries)
        {
            if (queries.Count() == 0)
            {
                return resource;
            }

            FormattersCollection formatters = api.Formatters;

            var query = resource + "?" + queries.Select(o => formatters.Find(o)
                                                                       .ToQuery(o))
                                                .Aggregate((o, p) => o += "&" + p);

            query = query.TrimEnd('&').TrimEnd('?');

            return query;
        }

        /// <summary>
        /// Map properties as <see cref="OrderByAttribute"/> <see cref="XmlElementAttribute"/> or <see cref="DataMemberAttribute"/> names
        /// </summary>
        /// <typeparam name="T">Type to map</typeparam>
        /// <param name="properties">Properties names to map</param>
        /// <returns>Mapped properties names</returns>
        public static ICollection<string> MapToApiProperties<T>(this ICollection<string> properties)
        {
            Type type = typeof(T);

            return properties.Select(o => MapToApiProperty<T>(o))
                             .Where(o => o != null)
                             .ToList();
        }

        public static ICollection<Filter> MapToApiProperties<T>(this ICollection<Filter> properties)
        {
            Type type = typeof(T);

            foreach (Filter filter in properties)
            {
                filter.Field = filter.Field.MapToApiProperty<T>();
            }

            return properties.Where(o => o.Field != null)
                             .ToList();
        }
       
        private static string MapToApiProperty<T>(this string property)
        {
            Type type = typeof(T);

            var propertyInfo = type.GetProperty(property, BindingFlags.Public | BindingFlags.Instance);

            if (propertyInfo == null)
            {
                // Leave untouched
                return property;
            }

            return propertyInfo.GetCustomAttribute<OrderByAttribute>()?.Name ??
                   propertyInfo.GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
                   propertyInfo.GetCustomAttribute<DataMemberAttribute>()?.Name;
        }
    }
}
