﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Formatters
{
    /// <summary>
    /// Gegenric formatter
    /// </summary>
    public interface IFormatter
    {
        /// <summary>
        /// Formats <paramref name="subject"/> to string query URL
        /// </summary>
        /// <param name="subject">Subjet ot be formatted</param>
        /// <returns>URL query</returns>
        string ToQuery(object subject);
    }
}
