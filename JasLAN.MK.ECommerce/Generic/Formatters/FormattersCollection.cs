﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Formatters
{
    /// <summary>
    /// Collection of formatters
    /// </summary>
    [Serializable]
    public class FormattersCollection : Dictionary<Type, Func<IFormatter>>
    {
        /// <summary>
        /// Instantinate a new object of <see cref="FormattersCollection"/>
        /// </summary>
        public FormattersCollection()
        {
        }

        /// <summary>
        /// Instantinate a new object of <see cref="FormattersCollection"/>
        /// </summary>
        /// <param name="dict">Dictionary to use</param>
        public FormattersCollection(IDictionary<Type, Func<IFormatter>> dict)
            : base(dict)
        {
        }

        /// <summary>
        /// Instantinate a new object of <see cref="FormattersCollection"/>
        /// </summary>
        /// <param name="serializationInfo">Serialization info</param>
        /// <param name="streamingContext">Streaming context</param>
        protected FormattersCollection(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        /// <summary>
        /// Find specific formatters
        /// </summary>
        /// <param name="formattable">Formattable objects</param>
        /// <returns>Formatter of given type</returns>
        public IFormatter Find(IUrlFormattable formattable)
        {
            if (formattable == null)
            {
                throw new ArgumentNullException(nameof(formattable));
            }

            foreach (var candidate in this)
            {
                if (candidate.Key.IsAssignableFrom(formattable.GetType()))
                {
                    return candidate.Value();
                }
            }

            throw new Exception($"Can not find formatter for: {typeof(IFormattable)}");
        }
    }
}
