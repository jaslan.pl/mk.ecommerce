﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Formatters
{
    /// <summary>
    /// Interface marking the class to be URL formattable
    /// </summary>
    public interface IUrlFormattable
    {
    }
}
