﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Formatters
{
    /// <summary>
    /// Specific formatter of <typeparamref name="T"/> subject
    /// </summary>
    /// <typeparam name="T">Type of subject to foratt</typeparam>
    public interface IFormatter<T> : IFormatter
    {
        /// <summary>
        /// Formats <paramref name="subject"/> to URL query
        /// </summary>
        /// <param name="subject">Subject to format</param>
        /// <returns>Valid URL query</returns>
        string ToQuery(T subject);
    }
}
