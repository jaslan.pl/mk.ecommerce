﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic.Formatters
{
    /// <summary>
    /// URL formatter implementation
    /// </summary>
    /// <typeparam name="T">Type of objects to format to URL</typeparam>
    public abstract class Formatter<T> : IFormatter<T>
    {
        /// <summary>
        /// Format object to query
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        public abstract string ToQuery(T subject);

        /// <summary>
        /// Format <paramref name="subject"/> to URL query
        /// </summary>
        /// <param name="subject">Subject to format</param>
        /// <returns>URL query</returns>
        public string ToQuery(object subject)
        {
            if (subject == null)
            {
                return null;
            }

            if (!typeof(T).IsAssignableFrom(subject.GetType()))
            {
                throw new ArgumentException(nameof(subject));
            }

            return ToQuery((T)subject);
        }
    }
}
