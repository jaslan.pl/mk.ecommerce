﻿using JasLAN.MK.ECommerce.Generic.Formatters;
using JasLAN.MK.ECommerce.Generic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasLAN.MK.ECommerce.Generic
{
    /// <summary>
    /// API E-Commerce interface
    /// </summary>
    public interface IApi
    {
        /// <summary>
        /// Gets or sets base shop URL
        /// </summary>
        string Url { get; set; }

        /// <summary>
        /// Gets or sets formatters used to format <see cref="PageQuery"/> and <see cref="FilterQuery"/>
        /// </summary>
        FormattersCollection Formatters { get; }

        /// <summary>
        /// Login to shop
        /// </summary>
        /// <param name="userLogin">User name</param>
        /// <param name="password">Password</param>
        /// <returns><c>true</c> if successfull</returns>
        bool Login(string userLogin, string password);

        /// <summary>
        /// Gets orders
        /// </summary>
        /// <param name="pageQuery">Page query</param>
        /// <param name="filterQuery">Filter query</param>
        /// <param name="orderStatusId">Order status ID</param>
        /// <returns>Orders</returns>
        IPageResponse<IOrder> GetOrders(PageQuery pageQuery, FilterQuery filterQuery, int orderStatusId);

        /// <summary>
        /// Get order statused
        /// </summary>
        /// <param name="pageQuery">Page query</param>
        /// <param name="filterQuery">Filter query</param>
        /// <returns></returns>
        IPageResponse<IOrderStatus> GetOrderStatuses(PageQuery pageQuery, FilterQuery filterQuery);

        /// <summary>
        /// Get order products
        /// </summary>
        /// <param name="orderId">Order from read order products</param>
        /// <param name="pageQuery">Page query</param>
        /// <returns></returns>
        IPageResponse<IOrderProduct> GetOrderProducts(int orderId, PageQuery pageQuery);

        /// <summary>
        /// Gets invoices
        /// </summary>
        /// <param name="pageQuery">Page query</param>
        /// <param name="filterQuery">Filter query</param>
        /// <returns>Invoices</returns>
        IPageResponse<IInvoice> GetInvoices(PageQuery pageQuery, FilterQuery filterQuery);
        
        /// <summary>
        /// Gets products
        /// </summary>
        /// <param name="onlyActive">Only active products</param>
        /// <param name="pageQuery">Page query</param>
        /// <param name="filterQuery">Filter query</param>
        /// <returns>Products</returns>
        IPageResponse<IProduct> GetProducts(bool onlyActive, PageQuery pageQuery, FilterQuery filterQuery);

        /// <summary>
        /// Gets product by <paramref name="code"/>
        /// </summary>
        /// <param name="code">Code</param>
        /// <returns>Product</returns>
        IProduct GetProductByCode(string code);

        /// <summary>
        /// Gets product by <paramref name="ean"/>
        /// </summary>
        /// <param name="ean">Ean</param>
        /// <returns>Product</returns>
        IProduct GetProductByEan(string ean);

        /// <summary>
        /// Update product stock
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="stockId"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        bool UpdateProductStock(int productId, int stockId, decimal stock);
    }
}
