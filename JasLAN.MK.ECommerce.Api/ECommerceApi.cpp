#include "stdafx.h"
#include "vcclr.h"
#include <stdio.h>
#include <string.h>

using namespace System::Runtime::InteropServices;
using namespace System::Net;

#include "ECommerceApi.h"

extern "C" int __declspec(dllexport) __cdecl GetLastException(char *message)
{
	try
	{
		if (Globals::lastException == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		NotImplementedException^ notImplementedException = dynamic_cast<NotImplementedException^>(Globals::lastException);

		if (notImplementedException != nullptr)
		{
			ExportString("Operacja nie jest obs�ugiwana", message);
		}
		else
		{
			ExportString(Globals::lastException->Message, message);
		}

		return S_OK;
	}
	catch (Exception^)
	{
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl GetLastWebException(char *message)
{
	try
	{
		if (Globals::lastWebException == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		ExportString(Globals::lastWebException->Message, message);

		return S_OK;
	}
	catch (Exception^)
	{
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl GetLastWebExceptionStatusCode(int *statusCode)
{
	try
	{
		if (Globals::lastWebException == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		ExportInt((int)((HttpWebResponse^)(Globals::lastWebException->Response))->StatusCode, statusCode);

		return S_OK;
	}
	catch (Exception^)
	{
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl SelectApi(int api)
{
	try
	{
		switch (api)
		{
		case API_SHOPER:
			Globals::Instance = gcnew JasLAN::MK::ECommerce::Shoper::ShoperApi;
			break;
		case API_PRESTASHOP:
			Globals::Instance = gcnew JasLAN::MK::ECommerce::PrestaShop::PrestaShopApi;
			break;
		case API_SKYSHOP:
			Globals::Instance = gcnew JasLAN::MK::ECommerce::SkyShop::SkyShopApi;
			break;
		default:
			return S_INDEX_OUT_OF_RANGE;
		}
	
		return S_OK;
	}
	catch (Exception^)
	{
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl Login(char *url, char *userLogin, char* password, int *result)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		String^ strUserLogin = gcnew String(userLogin);
		String^ strPassword = gcnew String(password);

		Globals::Instance->Url = gcnew String(url);
		bool response = Globals::Instance->Login(strUserLogin, strPassword);

		if (!response)
		{
			*result = 0;
			return S_OK;
		}

		*result = 1;
		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

/*
 *	Orders
 */

extern "C" int __declspec(dllexport) __cdecl GetOrders(int pageNumber, int pageSize, int *count, int *pageCount, int *pages, char *sortOrder, int statusId, char *filterField, char *filterOperator, char* filterValue)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		String^ strSortOrder = gcnew String(sortOrder);
		String^ strFilterField = gcnew String(filterField);
		String^ strFilterOperator = gcnew String(filterOperator);
		String^ strFilterValue = gcnew String(filterValue);

		FilterQuery^ filterQuery = gcnew FilterQuery();
		if (!String::IsNullOrEmpty(strFilterField) && !String::IsNullOrEmpty(strFilterValue))
		{
			filterQuery->Filters->Add(gcnew Filter(strFilterField, strFilterOperator, strFilterValue));
		}

		IPageResponse<IOrder^> ^orders = Globals::Instance->GetOrders(gcnew PageQuery(pageNumber, pageSize, strSortOrder), filterQuery, statusId);

		*count = orders->Count;
		*pageCount = orders->List->Length;
		*pages = orders->Pages;

		Globals::Orders = orders->List;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl ReadOrder(
	int index,
	int* orderId,
	char* date,
	char* deliveryDate,
	int* orderStatusId,
	long* total,
	long* totalShipping,
	char* email,
	char* notes,
	long* totalPaid,
	int* productsCount)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (index < 0 || index >= Globals::Orders->Length)
		{
			return S_INDEX_OUT_OF_RANGE;
		}

		IOrder^ order = Globals::Orders[index];

		ExportInt(order->OrderId, orderId);
		ExportString(order->Date, date);
		ExportString(order->DeliveryDate, deliveryDate);
		ExportInt(order->OrderStatusId, orderStatusId);
		ExportAmount(order->Total, total);
		ExportAmount(order->TotalShipping, totalShipping);
		ExportString(order->Email, email);
		ExportString(order->Notes, notes);
		ExportAmount(order->TotalPaid, totalPaid);
		ExportInt(order->ProductsCount, productsCount);

		Globals::Order = order;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl SetOrderBillingAddress()
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try 
	{
		Globals::OrderAddress = nullptr;

		if (Globals::Order == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		if (Globals::Order->BillingAddress == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		Globals::OrderAddress = Globals::Order->BillingAddress;

	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}

	return S_OK;
}

extern "C" int __declspec(dllexport) __cdecl SetOrderDeliveryAddress()
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try 
	{
		Globals::OrderAddress = nullptr;

		if (Globals::Order == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		if (Globals::Order->DeliveryAddress == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		Globals::OrderAddress = Globals::Order->DeliveryAddress;

	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}

	return S_OK;
}

extern "C" int __declspec(dllexport) __cdecl ReadOrderAddress(
	char *city,
	char *company,
	char *country,
	char *firstName,
	char *lastName,
	char *taxIdentificationNumber,
	char *phone,
	char *postcode,
	char *street)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (Globals::OrderAddress == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		IAddress^ address = Globals::OrderAddress;

		ExportString(address->City, city);
		ExportString(address->Company, company);
		ExportString(address->Country, country);
		ExportString(address->FirstName, firstName);
		ExportString(address->LastName, lastName);
		ExportString(address->TaxIdentificationNumber, taxIdentificationNumber);
		ExportString(address->Phone, phone);
		ExportString(address->Postcode, postcode);
		ExportString(address->Street, street);

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

/*
 *	OrderProducts
 */

extern "C" int __declspec(dllexport) __cdecl GetOrderProducts(int orderId, int pageNumber, int pageSize, int *count, int *pageCount, int *pages)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		IPageResponse<IOrderProduct^> ^orderProducts = Globals::Instance->GetOrderProducts(orderId, gcnew PageQuery(pageNumber, pageSize));

		*count = orderProducts->Count;
		*pageCount = orderProducts->List->Length;
		*pages = orderProducts->Pages;

		Globals::OrderProducts = orderProducts->List;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl ReadOrderProduct(
	long index,
	long *price,
	long *discount,
	long *quantity,
	char *name,
	char *code,
	char *tax,
	long *taxValue,
	char *unit)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (index < 0 || index >= Globals::OrderProducts->Length)
		{
			return S_INDEX_OUT_OF_RANGE;
		}

		IOrderProduct^ orderProduct = Globals::OrderProducts[index];
		Globals::OrderProduct = orderProduct;

		ExportAmount(orderProduct->Price, price);
		ExportAmount(orderProduct->Discount, discount);
		ExportAmount(orderProduct->Quantity, quantity);
		ExportString(orderProduct->Name, name);
		ExportString(orderProduct->Code, code);
		ExportString(orderProduct->Tax, tax);
		ExportAmount(orderProduct->TaxValue, taxValue);
		ExportString(orderProduct->Unit, unit);

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

/*
 * Statuses
 */

extern "C" int __declspec(dllexport) __cdecl GetStatuses(int pageNumber, int pageSize, int *count, int *pageCount, int *pages)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		IPageResponse<IOrderStatus^> ^orderStatuses = Globals::Instance->GetOrderStatuses(gcnew PageQuery(pageNumber, pageSize), gcnew FilterQuery());

		*count = orderStatuses->Count;
		*pageCount = orderStatuses->List->Length;
		*pages = orderStatuses->Pages;

		Globals::OrderStatuses = orderStatuses->List;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl ReadStatus(
	int index,
	int* statusId,
	char* name)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (index < 0 || index >= Globals::OrderStatuses->Length)
		{
			return S_INDEX_OUT_OF_RANGE;
		}

		IOrderStatus^ orderStatus = Globals::OrderStatuses[index];

		ExportInt(orderStatus->Id, statusId);
		ExportString(orderStatus->Name, name);

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

/*
 *	Invoices
 */

extern "C" int __declspec(dllexport) __cdecl GetInvoices(int pageNumber, int pageSize, int *count, int *pageCount, int *pages)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		FilterQuery^ filterQuery = gcnew FilterQuery();
		

		IPageResponse<IInvoice^> ^invoices = Globals::Instance->GetInvoices(gcnew PageQuery(pageNumber, pageSize), filterQuery);

		*count = invoices->Count;
		*pageCount = invoices->List->Length;
		*pages = invoices->Pages;

		Globals::Invoices = invoices->List;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl ReadInvoice(
	int index,
	char* number,
	char* date,
	char* saleDate,
	long* total,
	char* paymentForm,
	long* net23,
	long* vat23,
	long* net8,
	long* vat8)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (index < 0 || index >= Globals::Invoices->Length)
		{
			return S_INDEX_OUT_OF_RANGE;
		}

		IInvoice^ invoice = Globals::Invoices[index];

		ExportString(invoice->Number, number);
		ExportString(invoice->Date, date);
		ExportString(invoice->SaleDate, saleDate);
		ExportAmount(invoice->Total, total);
		ExportString(invoice->PaymentForm, paymentForm);
		ExportAmount(invoice->Net23, net23);
		ExportAmount(invoice->Vat23, vat23);
		ExportAmount(invoice->Net8, net8);
		ExportAmount(invoice->Vat8, vat8);

		Globals::Invoice = invoice;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl SetInvoiceBillingAddress()
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		Globals::InvoiceAddress = nullptr;

		if (Globals::Invoice == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		if (Globals::Invoice->BillingAddress == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		Globals::InvoiceAddress = Globals::Invoice->BillingAddress;

	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}

	return S_OK;
}

extern "C" int __declspec(dllexport) __cdecl ReadInvoiceAddress(
	char *city,
	char *company,
	char *country,
	char *firstName,
	char *lastName,
	char *taxIdentificationNumber,
	char *phone,
	char *postcode,
	char *street)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (Globals::InvoiceAddress == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		IAddress^ address = Globals::InvoiceAddress;

		ExportString(address->City, city);
		ExportString(address->Company, company);
		ExportString(address->Country, country);
		ExportString(address->FirstName, firstName);
		ExportString(address->LastName, lastName);
		ExportString(address->TaxIdentificationNumber, taxIdentificationNumber);
		ExportString(address->Phone, phone);
		ExportString(address->Postcode, postcode);
		ExportString(address->Street, street);

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

/*
 *	Products
 */

extern "C" int __declspec(dllexport) __cdecl GetProducts(int pageNumber, int pageSize, int *count, int *pageCount, int *pages, int onlyActive, char *sortOrder, char *filterField, char *filterOperator, char* filterValue)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		String^ strSortOrder = gcnew String(sortOrder);
		String^ strFilterField = gcnew String(filterField);
		String^ strFilterOperator = gcnew String(filterOperator);
		String^ strFilterValue = gcnew String(filterValue);

		FilterQuery^ filterQuery = gcnew FilterQuery();
		if (!String::IsNullOrEmpty(strFilterField) && !String::IsNullOrEmpty(strFilterValue))
		{
			filterQuery->Filters->Add(gcnew Filter(strFilterField, strFilterOperator, strFilterValue));
		}

		IPageResponse<IProduct^> ^products = Globals::Instance->GetProducts(onlyActive == 1, gcnew PageQuery(pageNumber, pageSize, strSortOrder), filterQuery);

		*count = products->Count;
		*pageCount = products->List->Length;
		*pages = products->Pages;

		Globals::Products = products->List;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl GetProductByCode(char *code)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		Globals::Products = gcnew array<IProduct ^>(0);
		String^ strCode = gcnew String(code);

		IProduct^ product = Globals::Instance->GetProductByCode(strCode);

		if (product == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		Globals::Products = gcnew array<IProduct ^>(1);
		Globals::Products[0] = product;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl GetProductByEan(char *ean)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		Globals::Products = gcnew array<IProduct ^>(0);
		String^ strEan = gcnew String(ean);

		IProduct^ product = Globals::Instance->GetProductByEan(strEan);

		if (product == nullptr)
		{
			return S_NO_DATA_TO_READ;
		}

		Globals::Products = gcnew array<IProduct ^>(1);
		Globals::Products[0] = product;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl ReadProduct(
	int index,
	int* productId,
	int* stockId,
	int* isActive,
	char* name,
	char* code,
	char* ean,
	long* price,
	long* price2,
	long* stock)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		if (index < 0 || index >= Globals::Products->Length)
		{
			return S_INDEX_OUT_OF_RANGE;
		}

		IProduct^ product = Globals::Products[index];

		ExportInt(product->ProductId, productId);
		ExportInt(product->StockId, stockId);
		ExportInt(product->IsActive, isActive);
		ExportString(product->Name, name);
		ExportString(product->Code, code);
		ExportString(product->Ean, ean);
		ExportAmount(product->Price, price);
		ExportAmount(product->Price2, price2);
		ExportAmount(product->Stock, stock);

		Globals::Product = product;

		return S_OK;
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}

extern "C" int __declspec(dllexport) __cdecl UpdateProductStock(
	int productId,
	int stockId,
	long stock)
{
	Globals::lastException = nullptr;
	Globals::lastWebException = nullptr;

	try
	{
		Decimal decStock = (Decimal)(stock) / 100;

		Boolean result = Globals::Instance->UpdateProductStock(productId, stockId, decStock);

		if (result)
		{
			return S_OK;
		}

		throw gcnew Exception("Nie mo�na zmieni� stanu");
	}
	catch (WebException^ ex)
	{
		Globals::lastWebException = ex;
		return S_WEB_EXCEPTION;
	}
	catch (Exception^ ex)
	{
		Globals::lastException = ex;
		return S_EXCEPTION;
	}
}