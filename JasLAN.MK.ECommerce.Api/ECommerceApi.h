// ECommerceApi.h

#pragma once

using namespace System;
using namespace System::Net;
using namespace JasLAN::MK::ECommerce::Generic;
using namespace JasLAN::MK::ECommerce::Generic::Model;

#define S_OK					0
#define S_EXCEPTION				1
#define S_INDEX_OUT_OF_RANGE	2
#define S_NO_DATA_TO_READ		3
#define S_WEB_EXCEPTION			4

#define API_SHOPER				0
#define API_PRESTASHOP			1
#define API_SKYSHOP				2

ref struct Globals {
	static IApi ^Instance = nullptr;

	static array<IOrder^> ^Orders;
	static IOrder ^Order;
	static IAddress ^OrderAddress;

	static array<IOrderProduct^> ^OrderProducts;
	static array<IOrderStatus^> ^OrderStatuses;
	static IOrderProduct ^OrderProduct;

	static array<IInvoice^> ^Invoices;
	static IInvoice ^Invoice;
	static IAddress ^InvoiceAddress;

	static array<IProduct^> ^Products;
	static IProduct ^Product;

	static Exception ^lastException;
	static WebException ^lastWebException;
};

void ExportString(System::String^ string, char *stringDestination)
{
	if (System::String::IsNullOrEmpty(string))
	{
		return;
	}

	System::IntPtr stringPointer = Marshal::StringToHGlobalAnsi(string);

	strcpy(stringDestination, (char *)(void *)stringPointer);

	Marshal::FreeHGlobal(stringPointer);
}

void ExportLong(long value, long *valueDestination)
{
	*valueDestination = value;
}

void ExportInt(int value, int *valueDestination)
{
	*valueDestination = value;
}

void ExportAmount(float value, long *valueDestination)
{
	*valueDestination = (long)(value * 100 + 0.5);
}

void ExportAmount(System::Decimal value, long *valueDestination)
{
	*valueDestination = (long)((float)value * 100 + 0.5);
}