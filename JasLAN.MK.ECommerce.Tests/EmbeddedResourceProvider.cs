﻿using System.IO;
using System.Reflection;

namespace JasLAN.MK.ECommerce.Tests
{
    internal static class EmbeddedResourceProvider
    {
        public static string Read(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"{typeof(EmbeddedResourceProvider).Namespace}.{fileName}";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
