﻿using System.Linq;
using JasLAN.MK.ECommerce.Generic.Model;
using JasLAN.MK.ECommerce.PrestaShop;
using NUnit.Framework;

namespace JasLAN.MK.ECommerce.Tests.PrestaShop
{ 
    [TestFixture]
    public class PrestaShopApiTest
    {
        private PrestaShopApi proxy;
                    
        [SetUp]
        public void Initialize()
        {
            this.proxy = new PrestaShopApi
            {
                Url = PrestaShopCredentials.Url
            };

            this.proxy.Login(PrestaShopCredentials.AppKey, string.Empty);
        }

        [Test]
        [Explicit]
        public void ICanGetOrderStatuses()
        {
            var orderStatuses = this.proxy.GetOrderStatuses(new PageQuery(1, 5, "id"), new FilterQuery());

            Assert.AreNotEqual(0, orderStatuses.Count);
            Assert.AreEqual(1, orderStatuses.Page);
            Assert.AreEqual(3, orderStatuses.Pages);
            Assert.AreNotEqual(0, orderStatuses.Count);
            Assert.AreNotEqual(0, orderStatuses.List.Count());
        }

        [Test]
        [Explicit]
        public void ICanGetOrders()
        {
            var orders = this.proxy.GetOrders(new PageQuery(1, 5, "id"), new FilterQuery(), 1);

            Assert.AreNotEqual(0, orders.Count);
            Assert.AreEqual(1, orders.Page);
            Assert.AreNotEqual(0, orders.Count);
            Assert.AreNotEqual(0, orders.List.Count());
        }
    }
}
