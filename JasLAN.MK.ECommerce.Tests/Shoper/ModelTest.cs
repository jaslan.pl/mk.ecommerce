﻿using JasLAN.MK.ECommerce.Shoper.Model;
using JasLAN.MK.ECommerce.Generic;
using NUnit.Framework;

namespace JasLAN.MK.ECommerce.Tests.Shoper
{
    [TestFixture]
    public class ModelTest
    {
        [Test]
        public void ICanReadOrder()
        {
            string json = EmbeddedResourceProvider.Read("Shoper.Order.json");

            Order order = json.ReadObject<Order>();

            Assert.IsNotNull(order);
            Assert.AreEqual(1, order.OrderId);
        }

        [Test]
        public void ICanReadProduct()
        {
            string json = EmbeddedResourceProvider.Read("Shoper.Product.json");

            Product product = json.ReadObject<Product>();

            Assert.IsNotNull(product);
            Assert.AreEqual(91, product.ProductId);
            Assert.AreEqual(48, product.Stock);
            Assert.AreEqual("Podkład higieniczny celulozowy szer. 60cm, 50mb", product.Name);
            Assert.AreEqual(18.5, product.Price);
            Assert.AreEqual(true, product.IsActive);
        }
    }
}
