﻿using System.Linq;
using JasLAN.MK.ECommerce.Shoper;
using JasLAN.MK.ECommerce.Generic.Model;
using NUnit.Framework;
using JasLAN.MK.ECommerce.Shoper.Model;

namespace JasLAN.MK.ECommerce.Tests.Shoper
{
    [TestFixture]
    public class ShoperApiTest
    {
        private ShoperApi api;

        [SetUp]
        public void Initialize()
        {
            this.api = new ShoperApi
            {
                Url = ShoperCredentials.Url
            };
            Assert.IsNotNull(this.api.Login(ShoperCredentials.User, ShoperCredentials.Password));
        }

        [Test]
        [Explicit]
        public void ICanGetOrders()
        {
            var orders = this.api.GetOrders(new PageQuery() { Order = { nameof(Order.Total) } }, new FilterQuery(), 0);

            Assert.AreNotEqual(0, orders.Count);
        }

        [Test]
        [Explicit]
        public void ICanGetOrdersWithFilter()
        {
            var statuses = this.api.GetOrderStatuses(new PageQuery(), new FilterQuery());

            Assert.AreNotEqual(0, statuses.Count);

            int statusId = statuses.List.Last().Id;

            var orders = this.api.GetOrders(new PageQuery(), new FilterQuery(), statusId);

            Assert.AreNotEqual(0, orders.Count);

            foreach (var order in orders.List)
            {
                Assert.AreEqual(statusId, order.OrderStatusId);
            }
        }

        [Test]
        [Explicit]
        public void ICanGetOrderProducts()
        {
            var orders = this.api.GetOrders(new PageQuery(), new FilterQuery(), 0);

            Assert.AreNotEqual(0, orders.Count);

            foreach (var order in orders.List.Take(3))
            {
                var orderProducts = this.api.GetOrderProducts(order.OrderId, new PageQuery());

                Assert.AreNotEqual(0, orderProducts.Count);
                Assert.AreEqual(order.ProductsCount, orderProducts.Count);
            }
        }

        [Test]
        [Explicit]
        public void ICanGetStatuses()
        {
            var statuses = this.api.GetOrderStatuses(new PageQuery(), new FilterQuery());

            Assert.AreNotEqual(0, statuses.Count);
        }

        [Test]
        [Explicit]
        public void ICanGetProducts()
        {
            var products = this.api.GetProducts(false, new PageQuery() { PageSize = 10, Order = { nameof(Product.Code) } }, new FilterQuery() {
               Filters = { new Filter(nameof(Product.Code), "~", "") }
            });

            Assert.AreNotEqual(0, products.Count);
        }

        [Test]
        [Explicit]
        public void ICanGetActiveProducts()
        {
            var products = this.api.GetProducts(true, new PageQuery() { Order = { nameof(Product.Ean) } }, new FilterQuery()
            {
                Filters = { new Filter(nameof(Product.Code), "~", "ABENA%") }
            });

            Assert.AreNotEqual(0, products.Count);

            foreach (var product in products.List)
            {
                Assert.AreEqual(true, product.IsActive);
            }
        }

        [Test]
        [Explicit]
        [TestCase("IM-DEZ-ML962")]
        public void ICanGetProductByCode(string code)
        {
            var product = this.api.GetProductByCode(code);

            Assert.IsNotNull(product);
            Assert.AreEqual(code, product.Code);
        }

        [Test]
        [Explicit]
        [TestCase(2212)]
        public void ICanGetProductByProductId(int productId)
        {
            var product = this.api.GetProductByProductId(productId);

            Assert.IsNotNull(product);
            Assert.AreEqual(productId, product.ProductId);
        }

        [Test]
        [Explicit]
        public void ICanUpdateProductStock()
        {
            this.api.UpdateProductStock(232, 0, 24);

            var product = this.api.GetProductByCode("IM-DEZ-ML962");

            Assert.IsNotNull(product);
            Assert.AreEqual("IM-DEZ-ML962", product.Code);
            Assert.AreEqual(24, product.Stock);
        }
    }
}
