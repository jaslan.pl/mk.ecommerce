﻿using JasLAN.MK.ECommerce.Generic;
using JasLAN.MK.ECommerce.Shoper.Model;
using NUnit.Framework;

namespace JasLAN.MK.ECommerce.Tests.Shoper
{
    [TestFixture]
    public class StatusTest
    {
        [Test]
        public void ICanReadStatus()
        {
            string json = "{\"status_id\":\"1\",\"active\":\"1\",\"default\":\"1\",\"color\":\"\",\"type\":\"1\",\"email_change\":\"0\",\"order\":\"1\",\"translations\":{\"pl_PL\":{\"trans_id\":\"1\",\"status_id\":\"1\",\"name\":\"z\u0142o\u017cone\",\"lang_id\":\"1\",\"message\":\"z\u0142o\u017cone\",\"message_html\":\"z\u0142o\u017cone\"}}}";

            Status status = json.ReadObject<Status>();

            Assert.IsNotNull(status);
            Assert.IsNotNull(status.Translations);
            Assert.AreNotEqual(0, status.Translations.Count);
            Assert.IsNotNull(status.Name);
        }
    }
}
