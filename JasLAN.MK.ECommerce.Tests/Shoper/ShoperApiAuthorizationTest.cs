﻿using JasLAN.MK.ECommerce.Shoper;
using NUnit.Framework;

namespace JasLAN.MK.ECommerce.Tests.Shoper
{
    [TestFixture]
    public class ShoperApiAuthorizationTest
    {
        private ShoperApi api;

        [Test]
        [Explicit]
        public void ICanAuthorize()
        {
            this.api = new ShoperApi
            {
                Url = ShoperCredentials.Url
            };
            Assert.IsNotNull(this.api.Login(ShoperCredentials.User, ShoperCredentials.Password));
        }

        [Test]
        [Explicit]
        public void ICanNotAuthorizeWithWrongCredentials()
        {
            this.api = new ShoperApi
            {
                Url = ShoperCredentials.Url
            };
            Assert.IsFalse(this.api.Login(ShoperCredentials.User, "ThisIsWrongCredential"));
        }

        [Test]
        [Explicit]
        public void ICanAuthorizeEvenWhenShopUrlHasMissingProtocol()
        {
            this.api = new ShoperApi
            {
                Url = ShoperCredentials.Url.Replace("https://", "").Replace("http://", "")
            };
            Assert.IsNotNull(this.api.Login(ShoperCredentials.User, ShoperCredentials.Password));
        }

        [Test]
        [Explicit]
        public void ICanNotAuthorizeWithIncorrectProtocol()
        {
            api = new ShoperApi();
            this.api.Url = ShoperCredentials.Url.Replace("https://", "ftp://").Replace("http://", "ftp://");

            if (this.api.Login(ShoperCredentials.User, ShoperCredentials.Password))
            {
                Assert.Fail();
            }
        }
    }
}
