﻿using System.Linq;
using JasLAN.MK.ECommerce.Generic.Model;
using JasLAN.MK.ECommerce.SkyShop;
using NUnit.Framework;

namespace JasLAN.MK.ECommerce.Tests.SkyShop
{
    [TestFixture]
    public class SkyShopApiTest
    {
        private SkyShopApi proxy;

        [SetUp]
        public void Initialize()
        {
            this.proxy = new SkyShopApi
            {
                Url = SkyShopCredentials.Url
            };
            this.proxy.Login(SkyShopCredentials.AppKey, string.Empty);
        }

        [Test]
        public void ICanGetOrderStatuses()
        {
            var orderStatuses = this.proxy.GetOrderStatuses(new PageQuery(1, 5, "OrderId"), new FilterQuery());

            Assert.AreNotEqual(0, orderStatuses.Count);
            Assert.AreEqual(1, orderStatuses.Page);
            Assert.AreEqual(2, orderStatuses.Pages);
            Assert.AreNotEqual(0, orderStatuses.Count);
            Assert.AreNotEqual(0, orderStatuses.List.Count());
        }

        [Test]
        [Explicit]
        public void ICanGetOrders()
        {
            var orders = this.proxy.GetOrders(new PageQuery(1, 5, "OrderId"), new FilterQuery(), 0);
            Assert.AreNotEqual(0, orders.Count);
            Assert.AreEqual(1, orders.Page);
            Assert.AreNotEqual(0, orders.Count);
            Assert.AreNotEqual(0, orders.List.Count());
        }

        [Test]
        [Explicit]
        public void ICanGetInvoices()
        {
            var invoices = this.proxy.GetInvoices(new PageQuery(1, 5, "InvoiceId"), new FilterQuery());
            Assert.AreNotEqual(0, invoices.Count);
            Assert.AreEqual(1, invoices.Page);
            Assert.AreNotEqual(0, invoices.Count);
            Assert.AreNotEqual(0, invoices.List.Count());
        }
    }
}
