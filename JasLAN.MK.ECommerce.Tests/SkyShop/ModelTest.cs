﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JasLAN.MK.ECommerce.Shoper.Model;
using JasLAN.MK.ECommerce.Generic;
using NUnit.Framework;
using System.Reflection;
using System.IO;
using JasLAN.MK.ECommerce.SkyShop.Model;
using JasLAN.MK.ECommerce.Skyshop;

namespace JasLAN.MK.ECommerce.Tests.SkyShop
{
    [TestFixture]
    public class ModelTest
    {
        [Test]
        public void ICanReadOrder()
        {
            string json = EmbeddedResourceProvider.Read("SkyShop.Orders.json");

            Orders orders = json.ReadSkyshopObject<Orders>();

            Assert.IsNotNull(orders);

            Assert.IsNotNull(orders);
            Assert.AreEqual(3, orders.Count);
        }

        [Test]
        public void ICanReadInvoice()
        {
            string json = EmbeddedResourceProvider.Read("SkyShop.Invoices.json");

            Invoices invoices = json.ReadSkyshopObject<Invoices>();

            Assert.IsNotNull(invoices);
            Assert.AreEqual(1, invoices.Count);

            var invoice = invoices.Values.Single();

            Assert.AreEqual(222.00, invoice.Total);
            Assert.AreEqual(180.49, invoice.TotalNet);
            Assert.AreEqual(41.51, invoice.TotalTax);
            Assert.AreEqual(180.49, invoice.Net23);
            Assert.AreEqual(41.51, invoice.Vat23);
        }
    }
}
